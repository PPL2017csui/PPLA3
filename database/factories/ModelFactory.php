<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'role' => $faker->jobTitle,
        'birth_date' => $faker->date,
        'phone' => $faker->phoneNumber,
        'points' => $faker->numberBetween($min = 100, $max = 1000),
    ];
});

$factory->define(App\Challenge::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'points' => $faker->numberBetween($min = 100, $max = 1000),
        'arrival_time' => $faker->time,
        'start_valid_date' => $faker->date,
        'end_valid_date' => $faker->date,
        'is_personal' => $faker->boolean,
    ];
});

$factory->define(App\History::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'time_checkin' => $faker->dateTime,
        'time_checkout' => $faker->dateTime,
        'long' => $faker->longitude,
        'lat' => $faker->latitude,
        'description' => $faker->text,
    ];
});

$factory->define(App\User_Subscribe::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'challenge_id' => function () {
            return factory(App\Challenge::class)->create()->id;
        },
        'is_subscribed' => $faker->boolean,
    ];
});

$factory->define(App\Log_point::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'challenge_name' => function () {
            return factory(App\Challenge::class)->create()->name;
        },
        'points' => $faker->numberBetween($min = 100, $max = 1000),
    ];
});
