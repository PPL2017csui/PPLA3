<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogPointsTable extends Migration
{
    public function up()
    {
        Schema::create('log_points', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('challenge_name');
            $table->integer('points');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('log_points');
    }
}