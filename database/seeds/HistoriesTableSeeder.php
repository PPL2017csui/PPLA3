<?php

use Illuminate\Database\Seeder;

class HistoriesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('histories')->insert([
            'user_id' => '1',
            'time_checkin' => '2017-03-29 11:48:00',
            'long' => '106.819488',
            'lat' => '-6.358505',
            'description' => 'kerja sampai jam 2 siang, izin ke rumah sakit',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43',
        ]);
        DB::table('histories')->insert([
            'user_id' => '3',
            'time_checkin' => '2017-03-29 11:48:00',
            'long' => '106.819488',
            'lat' => '-6.358505',
            'description' => 'lembur sampai malam hari ini',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('histories')->insert([
            'user_id' => '2',
            'time_checkin' => '2017-03-29 09:40:00',
            'long' => '106.819488',
            'lat' => '-6.358505',
            'description' => 'kerjaan ada yang kurang, sampai jam 12 sing',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('histories')->insert([
            'user_id' => '4',
            'time_checkin' => '2017-03-29 10:32:00',
            'long' => '106.819488',
            'lat' => '-6.358505',
            'description' => 'kerja sampai jam 4',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('histories')->insert([
            'user_id' => '5',
            'time_checkin' => '2017-03-29 11:30:00',
            'long' => '106.819488',
            'lat' => '-6.358505',
            'description' => 'Terlambat sedikit karena macet',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
    }
}
