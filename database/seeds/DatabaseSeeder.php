<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call('UsersTableSeeder');
        $this->call('ChallengesTableSeeder');
        $this->call('HistoriesTableSeeder');
        $this->call('Log_pointsTableSeeder');
        $this->call('Users_SubscribesTableSeeder');
    }
}
