<?php

use Illuminate\Database\Seeder;

class Log_pointsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('log_points')->insert([
            'user_id' => '1',
            'challenge_name' => 'Basic 1',
            'points' => 150,
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('log_points')->insert([
            'user_id' => '2',
            'challenge_name' => 'Basic 1',
            'points' => 150,
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('log_points')->insert([
            'user_id' => '3',
            'challenge_name' => 'Basic 1',
            'points' => 150,
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('log_points')->insert([
            'user_id' => '4',
            'challenge_name' => 'Basic 1',
            'points' => 150,
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('log_points')->insert([
            'user_id' => '5',
            'challenge_name' => 'Basic 1',
            'points' => 150,
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
    }
}
