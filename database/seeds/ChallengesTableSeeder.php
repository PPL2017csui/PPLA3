<?php

use Illuminate\Database\Seeder;

class ChallengesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('challenges')->insert([
            'name' => 'Basic 1',
            'description' => 'Be on time and you will get +10 points!',
            'points' => '10',
            'arrival_time' => '08:00:00',
            'start_valid_date' => '2017-05-07',
            'end_valid_date' => '2017-12-31',
            'is_personal' => 'FALSE',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('challenges')->insert([
            'name' => 'Basic 2',
            'description' => 'Challenge your friend to arrive before you!',
            'points' => '25',
            'arrival_time' => '09:00:00',
            'start_valid_date' => '2017-05-07',
            'end_valid_date' => '2017-12-31',
            'is_personal' => 'FALSE',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('challenges')->insert([
            'name' => 'Basic 3',
            'description' => 'Finish your work before 3 PM!',
            'points' => '15',
            'arrival_time' => '08:30:00',
            'start_valid_date' => '2017-05-07',
            'end_valid_date' => '2017-12-31',
            'is_personal' => 'FALSE',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
    }
}
