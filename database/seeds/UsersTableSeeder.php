<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Agung Ahmad',
            'email' => 'agungahmad@gmail.com',
            'role' => 'admin',
            'birth_date' => '1996-01-01',
            'phone' => '123456789',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('users')->insert([
            'name' => 'Angga Arifandi',
            'email' => 'namapanjanggua@gmail.com',
            'role' => 'admin',
            'birth_date' => '1996-01-02',
            'phone' => '987654321',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('users')->insert([
            'name' => "Harits Nur Fauzan",
            'email' => 'affandayana@gmail.com',
            'role' => 'admin',
            'birth_date' => '1996-01-03',
            'phone' => '567894321',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('users')->insert([
            'name' => 'Johannes Tobing',
            'email' => 'johannse.bin419@gmail.com',
            'role' => 'admin',
            'birth_date' => '1996-01-05',
            'phone' => '987654321',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('users')->insert([
            'name' => "Hadi Syah Putra",
            'email' => 'crunry@gmail.com',
            'role' => 'admin',
            'birth_date' => '1996-02-03',
            'phone' => '567894321',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
    }
}
