<?php

use Illuminate\Database\Seeder;

class Users_SubscribesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_subscribes')->insert([
            'user_id' => '5',
            'challenge_id' => '1',
            'is_subscribed' => 'TRUE',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('users_subscribes')->insert([
            'user_id' => '2',
            'challenge_id' => '1',
            'is_subscribed' => 'TRUE',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('users_subscribes')->insert([
            'user_id' => '1',
            'challenge_id' => '3',
            'is_subscribed' => 'TRUE',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('users_subscribes')->insert([
            'user_id' => '2',
            'challenge_id' => '1',
            'is_subscribed' => 'TRUE',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
        DB::table('users_subscribes')->insert([
            'user_id' => '1',
            'challenge_id' => '1',
            'is_subscribed' => 'FALSE',
            'created_at' => '2017-04-25 17:38:43',
            'updated_at' => '2017-04-25 17:38:43'
        ]);
    }
}
