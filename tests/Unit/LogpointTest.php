<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\ChallengeController;
use App\log_point;

class Log_pointTest extends TestCase
{
    //use DatabaseMigrations;

    public function testHasAttribute()
    {
        $log_point = new log_point();
        $this->assertTrue($log_point->has('challenge_name'));
    }

    public function testUser()
    {
        $log_point = log_point::whereHas('user');
        $this->assertNotNull($log_point);
    }

    public function testStore()
    {
        $log_point = factory(\App\Log_point::class)->make();
        $response = $this->call('POST', 'api/log_points', ['user_id' => $log_point->user_id, 'challenge_name' => $log_point->challenge_name, 'points' => $log_point->points]);
    }
}