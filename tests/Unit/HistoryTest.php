<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\JsonResponse;
use App\Controllers\HistoryController;
use App\History;
use App\User;
use Carbon\Carbon;
use App\Challenge;
use App\User_Subscribe;
use DB;
use Artisan;

class HistoryTest extends TestCase
{
    //use DatabaseMigrations;

    public function testTableIsMatched()
    {
        $history = new History();
        $this->assertEquals('histories', $history->getTable());
    }

    public function testHasAttribute()
    {
        $history = new History();
        $this->assertTrue($history->has('user_id'));
        $this->assertFalse($history->has('email'));
    }

    public function testGetAllAttendances()
    {
        factory(\App\History::class)->create();
        $response = $this->call('GET', 'api/histories');
        $this->assertEquals(200, $response->status());
    }

    public function testGetNoAttendances()
    {
        DB::table('histories')->truncate();
        $response = $this->call('GET', 'api/histories');
        $this->assertEquals(200, $response->status());
        Artisan::call('db:seed', ['--class' => 'HistoriesTableSeeder']);

    }

    public function testSubmit()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $response = $this->call('POST', 'api/histories/submit',
            ['user_id' => $user_id, 'lat' => '10', 'long' => '11', 'description' => 'testapisubmit']);
        $this->assertEquals(201, $response->status());

        $user_id = factory(\App\User::class)->create()->id;
        $challenge_id = factory(\App\Challenge::class)->create([
            'arrival_time' => '23:59:59',
            'start_valid_date' => Carbon::now()->toDateString(),
            'end_valid_date' => Carbon::now()->toDateString(),
            'is_personal' => false,
        ])->id;

        factory(\App\User_Subscribe::class)->create([
            'challenge_id' => $challenge_id,
            'user_id' => $user_id,
            'is_subscribed' => true,
        ]);
        $response = $this->call('POST', 'api/histories/submit',
            ['user_id' => $user_id, 'lat' => '10', 'long' => '11', 'description' => 'testapisubmit']);
        $this->assertEquals(201, $response->status());
    }

    public function testCheckout()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $history_id = factory(\App\History::class)->create([
            'user_id' => $user_id,
            'time_checkin' => Carbon::now()->toDayDateTimeString(),
            'time_checkout' => null,
        ])->id;
        $response = $this->call('POST', 'api/histories/checkout', ['id' => $history_id]);
        $this->assertEquals(200, $response->status());

        $user_id = factory(\App\User::class)->create()->id;
        $history_id = factory(\App\History::class)->create([
            'user_id' => $user_id,
            'time_checkin' => Carbon::now()->subMinutes(10),
            'time_checkout' => null,
        ])->id;
        $response = $this->call('POST', 'api/histories/checkout', ['id' => $history_id]);
        $this->assertEquals(201, $response->status());

    }

    public function testMyHistory()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $year = Carbon::now()->year;
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-01-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-02-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-03-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-04-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-05-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-06-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-07-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-08-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-09-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-10-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-11-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-12-26 16:28:38',
        ]);
        $response = $this->call('POST', 'api/histories/myhistory', ['user_id' => $user_id]);
        $this->assertEquals(200, $response->status());
    }

     public function testMyNullHistory()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $year = Carbon::now()->year;
        DB::table('histories')->truncate();
        $response = $this->call('POST', 'api/histories/myhistory', ['user_id' => $user_id]);
        $this->assertEquals(200, $response->status());
        Artisan::call('db:seed', ['--class' => 'HistoriesTableSeeder']);
    }

    public function testUser()
    {
        $history = History::whereHas('user');
        $this->assertNotNull($history);
    }
}