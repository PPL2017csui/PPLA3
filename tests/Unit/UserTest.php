<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\JsonResponse;
use App\Controllers\UserController;
use App\User;
use App\Challenge;
use App\User_Subscribe;
use Session;

class UserTest extends TestCase
{
    //use DatabaseMigrations;

    public function testLogin()
    {
        $user = factory(\App\User::class)->create();
        $response = $this->call('POST', 'api/user/login',
            ['email' => $user->email, 'name' => $user->name, 'birth_date' => $user->birth_date]);
        $this->assertEquals(200, $response->status());

        $user = factory(\App\User::class)->make();
        $response = $this->call('POST', 'api/user/login',
            ['email' => $user->email, 'name' => $user->name, 'birth_date' => $user->birth_date]);
        $this->assertEquals(201, $response->status());
    }

    public function testHistory()
    {
        $user = User::whereHas('history');
        $this->assertNotNull($user);
    }

    public function testLog_point()
    {
        $user = User::whereHas('log_point');
        $this->assertNotNull($user);
    }

    public function testSubscribes()
    {
        $user = User::whereHas('subscribes', function ($q) {
            $q->where('user_id', '1');
        });
        $this->assertNotNull($user);
    }

    public function testChallenger()
    {
        $user = User::whereHas('challenger', function ($q) {
            $q->where('challenger_id', '1');
        });
        $this->assertNotNull($user);
    }

    public function testChallenged()
    {
        $user = User::whereHas('challenged', function ($q) {
            $q->where('challenged_id', '2');
        });
        $this->assertNotNull($user);
    }

    public function testGetProfile()
    {
        $response = $this->call('POST', 'api/user/getProfile', ['id' => 1]);
        $this->assertEquals(200, $response->status());
    }

    public function testHasAttribute()
    {
        $user = new User();
        $this->assertTrue($user->has('email'));
        $this->assertFalse($user->has('address'));
    }

    public function testTableIsMatched()
    {
        $user = new User();
        $this->assertEquals('users', $user->getTable());
    }

    public function testSetProfile()
    {
        $id = factory(\App\User::class)->create()->id;
        $response = $this->call('POST', 'api/user/setProfile',
            ['id' => $id, 'name' => 'GantiNama', 'phone' => '089678316883']);
        $this->assertEquals(201, $response->status());
    }

    public function testGetPoints()
    {
        $user_id = factory(\App\Log_point::class)->create()->user_id;
        $log_point = factory(\App\Log_point::class)->create([
            'user_id' => $user_id,
        ]);
        $response = $this->call('POST', 'api/user/getPoints', ['user_id' => $user_id]);
        $this->assertEquals(200, $response->status());
    }

    public function testViewUser(){
         $this->withoutMiddleware();
         $res = $this->call('GET','/employees');
    }

    public function testViewEmployees(){
         $this->withoutMiddleware();
         $res = $this->call('GET','/employees');
    }

    public function testLoginPostFailed(){
        Session::start();
        $response = $this->call('POST', '/', [
            'username' => 'adminSalah',
            'password' => 'password0!',
            '_token' => csrf_token()
        ]);
    
    }
    
    public function testLoginPostSuccess(){
        Session::start();
        $response = $this->call('POST', '/', [
            'username' => 'admin',
            'password' => 'password0!',
            '_token' => csrf_token()
        ]);
    
    }

    public function testEditEmployeeView(){
        $this->withoutMiddleware();

        $response = $this->call('GET', '/employees/1');
    
    }

    public function testDeleteEmployeeView(){
        $this->withoutMiddleware();
        $id = factory(\App\User::class)->create()->id;

        $response = $this->call('GET', '/employees-delete/'.$id);
    
    }

    public function testUpdateUser(){
        $this->withoutMiddleware();
        $user = factory(\App\User::class)->create();
        $id = $user->id;
        $response = $this->call('POST', '/employees/'.$id, [
            'name' => 'admin',
            'email' => $user->email,
            'role' => $user->role,
            'birth_date' => $user->birth_date,
            'phone' => $user->phone,
            'id' => $user->id,
            '_token' => csrf_token()
        ]);
    
    }

}