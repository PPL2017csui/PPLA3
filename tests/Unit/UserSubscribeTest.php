<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Http\Controllers\User_SubscribeController;
use App\User_Subscribe;
use App\User;
use App\Challenge;
use Carbon\Carbon;
use App\History;

class User_SubscribeTest extends TestCase
{
    //use DatabaseMigrations;

    public function testTableIsMatched()
    {
        $User_subscribe = new User_Subscribe();
        $this->assertEquals('users_subscribes', $User_subscribe->getTable());
    }

    public function testHasAttribute()
    {
        $user_subscribe = new User_Subscribe();
        $this->assertTrue($user_subscribe->has('challenge_id'));
        $this->assertFalse($user_subscribe->has('email'));
    }

/*    public function testCheck_Win()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $challenge_id = factory(\App\Challenge::class)->create([
            ''
        ])->id;
        $date_checkin = Carbon::now();
        $date_checkin->setTime(07, 30, 00);

        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'time_checkin' => $date_checkin,
        ]);

        factory(\App\User_Subscribe::class)->create([
            'challenge_id' => $challenge_id,
            'user_id' => $user_id,
            'is_subscribed' => TRUE,
        ]);

        $response = $this->call('POST', 'api/users_subscribers/check_win', ['user_id' => $user_id, 'challenge_id' => $challenge_id, 'date' => $date_checkin]);
        $this->assertEquals(201, $response->status());
    }
*/
    public function testSubscribe()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $challenge_id = factory(\App\Challenge::class)->create()->id;
        $response = $this->call('POST', 'api/users_subscribers/subscribe',
            ['challenge_id' => $challenge_id, 'user_id' => $user_id]);

        $user_id = factory(\App\User::class)->create()->id;
        $challenge_id = factory(\App\Challenge::class)->create()->id;
        factory(\App\User_Subscribe::class)->create([
            'challenge_id' => $challenge_id,
            'user_id' => $user_id,
            'is_subscribed' => false,
        ]);
        $response = $this->call('POST', 'api/users_subscribers/subscribe',
            ['challenge_id' => $challenge_id, 'user_id' => $user_id]);

        $user_id2 = factory(\App\User::class)->create()->id;
        $challenge_id2 = factory(\App\Challenge::class)->create()->id;
        $user_subscribe = new User_Subscribe();
        $user_subscribe->challenge_id = $challenge_id2;
        $user_subscribe->user_id = $user_id2;
        $user_subscribe->is_subscribed = FALSE;
        $user_subscribe->save();
        $response = $this->call('POST', 'api/users_subscribers/subscribe', ['challenge_id' => $challenge_id2, 'user_id' => $user_id2]);
        $this->assertEquals(201, $response->status());
    }

    public function testUnsubscribe()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $challenge_id = factory(\App\Challenge::class)->create()->id;
        $user_subscribe_id = factory(\App\User_Subscribe::class)->create([
            'challenge_id' => $challenge_id,
            'user_id' => $user_id,
            'is_subscribed' => true,
        ])->id;
        $response = $this->call('POST', 'api/users_subscribers/unsubscribe', ['user_id' => $user_subscribe_id, 'challenge_id' => $challenge_id]);
    //    $this->assertEquals(201, $response->status());
    }

    public function testUser()
    {
        $user_subscribe = User_Subscribe::whereHas('user');
        $this->assertNotNull($user_subscribe);
    }

    public function testChallenge()
    {
        $user_subscribe = User_Subscribe::whereHas('challenge');
        $this->assertNotNull($user_subscribe);
    }

    public function testGetSubscribers()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $challenge_id = factory(\App\Challenge::class)->create()->id;
        $user_subscribe = new User_Subscribe();
        factory(\App\User_Subscribe::class)->create([
            'challenge_id' => $challenge_id,
            'user_id' => $user_id,
            'is_subscribed' => true,
        ]);
        $response = $this->call('POST', 'api/users_subscribers/getSubscribers', ['challenge_id' => $challenge_id]);
        $this->assertEquals(200, $response->status());

        $challenge_id = factory(\App\Challenge::class)->create()->id;
        $response = $this->call('POST', 'api/users_subscribers/getSubscribers', ['challenge_id' => $challenge_id]);
        $this->assertEquals(200, $response->status());
    }


    public function testGetSubscribedChallenges()
    {
        $user_id = factory(\App\User::class)->create()->id;
        $challenge_id = factory(\App\Challenge::class)->create()->id;
        factory(\App\User_Subscribe::class)->create([
            'challenge_id' => $challenge_id,
            'user_id' => $user_id,
            'is_subscribed' => TRUE,
        ]);
        $response = $this->call('POST', 'api/users_subscribers/getSubscribedChallenges', ['user_id' => $user_id]);
     //   $this->assertEquals(200, $response->status());
    }
}