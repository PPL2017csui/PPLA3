<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\ChallengeController;
use App\Challenge;
use App\User;
use Carbon\Carbon;

class ChallengeTest extends TestCase
{
    //use DatabaseMigrations;

    public function testGetAllChallenges()
    {
        $response = $this->call('GET', 'api/challenges');
     // $this->assertEquals(200, $response->status());
        factory(\App\Challenge::class)->create([
            'start_valid_date' => Carbon::now()->toDateString(),
            'end_valid_date' => Carbon::now()->toDateString(),
            'is_personal' => false,
        ]);
        $response = $this->call('GET', 'api/challenges');
     // $this->assertEquals(200, $response->status());
    }

    public function testHasAttribute()
    {
        $challenge = new Challenge();
        $this->assertTrue($challenge->has('description'));
        $this->assertFalse($challenge->has('email'));
    }

    public function testTableIsMatched()
    {
        $challenge = new Challenge();
        $this->assertEquals('challenges', $challenge->getTable());
    }

    public function testSubscribed()
    {
        $challenge = Challenge::whereHas('subscribed', function ($q) {
            $q->where('challenge_id', '1');
        });
        $this->assertNotNull($challenge);
    }

    public function testChallenge()
    {
        $challenge = Challenge::whereHas('challenge');
        $this->assertNotNull($challenge);
    }

  

    public function testUpdateChallenge()
    {
        $challenge = factory(\App\Challenge::class)->create();
        $name = $challenge->name;
        $challenge_id = $challenge->id;
        $desc = $challenge->description;
        $points = $challenge->points;
        $end_valid_date = $challenge->end_valid_date;
        $start_valid_date = $challenge->start_valid_date;
        $is_personal = $challenge->is_personal;
        $arrival_time = $challenge->arrival_time;
        $update_challenge_id = $challenge->create([
            'name' => $name,
            'description' => $desc,
            'points' => $points,
            'end_valid_date' => $end_valid_date,
            'start_valid_date' => $start_valid_date,
            'is_personal' => $is_personal,
            'arrival_time' => $arrival_time,
        ])->id;
        $response = $this->call('POST', 'api/ChallengeController',
          [
            'id' => $challenge_id,
            'name' => $name,
            'description' => $desc,
            'points' => $points,
            'end_valid_date' => $end_valid_date,
            'start_valid_date' => $start_valid_date,
            'is_personal' => $is_personal,
            'arrival_time' => $arrival_time,
          ]);
     //   $this->assertEquals(201, $response->status());
    }

    public function testViewChallenge(){
         $this->withoutMiddleware();
         $res = $this->call('GET','/challenges');
    }

    public function testViewIndex(){
         $this->withoutMiddleware();
         $year = Carbon::now()->year;
         $user_id = factory(\App\User::class)->create()->id;
         factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-01-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-02-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-03-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-04-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-05-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-06-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-07-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-08-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-09-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-10-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-11-26 16:28:38',
        ]);
        factory(\App\History::class)->create([
            'user_id' => $user_id,
            'created_at' => $year . '-12-26 16:28:38',
        ]);
         $res = $this->call('GET','/index');
    }

    public function testViewLogin2(){
         $res = $this->call('GET','/challenges');
    }

    public function testViewLogin(){
         $res = $this->call('GET','/');
    }

    public function testEditChallengeView(){
        $this->withoutMiddleware();
        $id = factory(\App\Challenge::class)->create()->id;
        $response = $this->call('GET', '/challenges-edit/'.$id);
    
    }

    public function testDeleteChallengeView(){
        $this->withoutMiddleware();
        $id = factory(\App\Challenge::class)->create()->id;
        $response = $this->call('GET', '/challenges-delete/'.$id);
    
    }

}
