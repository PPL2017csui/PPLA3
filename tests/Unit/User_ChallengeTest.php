<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\User_ChallengeController;
use App\User_Challenge;
use App\User;
use App\Challenge;

class User_ChallengeTest extends TestCase
{
    //use DatabaseMigrations;
    
    // public function testGetAllUser_Challenge()
    // {
    //     $response = $this->call('GET', 'api/users_challenges');
    //     $this->assertEquals(200, $response->status());
    // }
    

    public function testTableIsMatched()
    {
        $user_challenge = new User_Challenge();
        $this->assertEquals('users_challenges', $user_challenge->getTable());
    }

    public function testHasChallenge_Id()
    {
        $user_challenge = new User_Challenge();
        $this->assertTrue($user_challenge->has('challenge_id'));
        $this->assertFalse($user_challenge->has('email'));
    }
    
   //  public function testSubscribe()
   //  {
   //      $challenge_id = factory(\App\Challenge::class)->create()->id;
   //      $user_id = factory(\App\User::class)->create()->id;

   //      $response = $this->call('POST', 'api/users_challenges/subscribe_unsubscribe',
   //          ['challenge_id' => $challenge_id, 'user_id' => $user_id, 'flag' => 0]);
   //      $this->assertEquals(201, $response->status());


   //      $challenge_id = factory(\App\Challenge::class)->create()->id;
   //      $user_id = factory(\App\User::class)->create()->id;
   //      $user_challenge = new User_Challenge();
   //      $user_challenge->user_id = $user_id;
   //      $user_challenge->challenge_id = $challenge_id;
   //      $user_challenge->is_daily = false;
   //      $user_challenge->is_subscribed = false;

   //      $user_challenge->save();

   //      $response = $this->call('POST', 'api/users_challenges/subscribe_unsubscribe',
   //          ['challenge_id' => $challenge_id, 'user_id' => $user_id, 'flag' => 0]);
   //      $this->assertEquals(201, $response->status());
   //  }

   // public function testUnsubscribe()
   //  {
   //      $challenge_id = factory(\App\Challenge::class)->create()->id;
   //      $user_id = factory(\App\User::class)->create()->id;

   //      $response = $this->call('POST', 'api/users_challenges/subscribe_unsubscribe',
   //          ['challenge_id' => $challenge_id, 'user_id' => $user_id, 'flag' => 1]);
   //      $this->assertEquals(201, $response->status());


   //      $challenge_id = factory(\App\Challenge::class)->create()->id;
   //      $user_id = factory(\App\User::class)->create()->id;
   //      $user_challenge = new User_Challenge();
   //      $user_challenge->user_id = $user_id;
   //      $user_challenge->challenge_id = $challenge_id;
   //      $user_challenge->is_daily = false;
   //      $user_challenge->is_subscribed = false;

   //      $user_challenge->save();

   //      $response = $this->call('POST', 'api/users_challenges/subscribe_unsubscribe',
   //          ['challenge_id' => $challenge_id, 'user_id' => $user_id, 'flag' => 1]);
   //      $this->assertEquals(201, $response->status());
   //  }
    
    public function testChallenger()
    {
        $user_challenge = User_Challenge::whereHas('challenger');
        $this->assertNotNull($user_challenge);
    }

    public function testChallenged()
    {
        $user_challenge = User_Challenge::whereHas('challenged');
        $this->assertNotNull($user_challenge);
    }

    public function testChallenge()
    {
        $user_challenge = User_Challenge::whereHas('challenge');
        $this->assertNotNull($user_challenge);
    }
}