package com.jojonomic.absenin.ModelTest;

import com.jojonomic.absenin.entity.Attendance;
import com.jojonomic.absenin.entity.User;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by ANGGA on 4/13/2017.
 */

public class UserTest {

    @Test
    public void nullTest() throws Exception {
        User user = new User();
        assertNull(user.getName());
        assertNull(user.getEmail());
        assertNull(user.getPhotoUrl());
    }

    @Test
    public void equalTestWithEmptyConstructor() throws Exception {
        User user = new User();
        user.setName("Angga");
        user.setEmail("anggadwiarifandi96@gmail.com");
        user.setPhotoUrl("http://www.google.com");
        user.setHasAttend(false);
        assertEquals(user.getName(),"Angga");
        assertEquals(user.getEmail(),"anggadwiarifandi96@gmail.com");
        assertEquals(user.getPhotoUrl(),"http://www.google.com");
        assertEquals(user.hasAttend(),false);
    }

    @Test
    public void equalTestWithConstructor() throws Exception {
        User user = new User("anggadwiarifandi96@gmail.com","Angga","http://www.google.com",false);
        assertEquals(user.getName(),"Angga");
        assertEquals(user.getEmail(),"anggadwiarifandi96@gmail.com");
        assertEquals(user.getPhotoUrl(),"http://www.google.com");
        assertEquals(user.hasAttend(),false);
    }

}
