package com.jojonomic.absenin.ActivityTest;

import com.jojonomic.absenin.BuildConfig;
import com.jojonomic.absenin.SplashScreen;
import com.jojonomic.absenin.SubmitAttendanceActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowLog;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by ANGGA on 4/15/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SubmitAttendanceActivityTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Mock
    SubmitAttendanceActivity activity;

    @Before
    public void setUpActivity(){
        ShadowLog.stream = System.out;
        activity = Robolectric.buildActivity(SubmitAttendanceActivity.class).create().resume().get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull(activity);
    }

    @Test
    public void testSubmit(){
        activity.submitAttendanceToServer(1,106.828689,-6.364601,"description");
    }

}
