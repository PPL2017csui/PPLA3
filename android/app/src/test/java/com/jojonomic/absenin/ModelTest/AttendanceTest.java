package com.jojonomic.absenin.ModelTest;

import com.jojonomic.absenin.entity.Attendance;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by ANGGA on 4/13/2017.
 * Test class for Attendance entity
 *
 */

public class AttendanceTest {

    @Test
    public void nullTest() throws Exception {
        Attendance attendance = new Attendance();
        assertNull(attendance.getName());
        assertNull(attendance.getTime());
    }

    @Test
    public void equalTestWithEmptyConstructor() throws Exception {
        Attendance attendance = new Attendance();
        attendance.setName("Angga");
        attendance.setTime("08.00");
        assertEquals(attendance.getName(),"Angga");
        assertEquals(attendance.getTime(),"08.00");
    }

    @Test
    public void equalTestWithConstructor() throws Exception {
        Attendance attendance = new Attendance("Angga","08.00");
        assertEquals(attendance.getName(),"Angga");
        assertEquals(attendance.getTime(),"08.00");
    }
}
