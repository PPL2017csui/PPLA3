package com.jojonomic.absenin.ActivityTest;

import com.jojonomic.absenin.AboutActivity;
import com.jojonomic.absenin.BuildConfig;
import com.jojonomic.absenin.SplashScreen;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by ANGGA on 4/27/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class AboutActivityTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Mock
    AboutActivity activity;

    @Before
    public void setUpActivity(){
        activity = Robolectric.buildActivity(AboutActivity.class).create().resume().get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull(activity);
    }

}
