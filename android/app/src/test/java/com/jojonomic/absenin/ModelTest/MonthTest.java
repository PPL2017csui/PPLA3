package com.jojonomic.absenin.ModelTest;

import com.jojonomic.absenin.entity.Attendance;
import com.jojonomic.absenin.entity.Month;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by ANGGA on 4/13/2017.
 */

public class MonthTest {

    @Test
    public void nullTest() throws Exception {
        Month month = new Month();
        assertNull(month.getName());
    }

    @Test
    public void equalTestWithEmptyConstructor() throws Exception {
        Month month = new Month();
        month.setName("Januari");
        assertEquals(month.getName(),"Januari");
    }

    @Test
    public void equalTestWithConstructor() throws Exception {
        Month month = new Month("Januari");
        assertEquals(month.getName(),"Januari");
    }

}
