package com.jojonomic.absenin.ActivityTest;

import com.android.volley.RequestQueue;
import com.google.android.gms.common.api.Api;
import com.jojonomic.absenin.BuildConfig;
import com.jojonomic.absenin.MainActivity;
import static org.mockito.Mockito.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by ANGGA on 4/15/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MainActivityTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Mock
    MainActivity activity;
    @Mock
    RequestQueue requestQueue;

    @Before
    public void setUpActivity(){
        activity = Robolectric.buildActivity(MainActivity.class).create().resume().get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull(activity);
    }

    @Test
    public void testCheckout(){
        activity.checkOut(1);
    }

    @Test
    public void testLogout(){
        activity.logout();
    }

}
