package com.jojonomic.absenin.ModelTest;

import com.jojonomic.absenin.BuildConfig;
import com.jojonomic.absenin.SplashScreen;
import com.jojonomic.absenin.entity.Attendance;
import com.jojonomic.absenin.entity.Challenge;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * Created by ANGGA on 4/27/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class ChallengeTest {

    @Test
    public void nullTest() throws Exception {
        Challenge challenge = new Challenge();
        assertNull(challenge.getDescription());
        assertNull(challenge.getRules());
    }

    @Test
    public void equalTestWithEmptyConstructor() throws Exception {
        Challenge challenge = new Challenge();
        challenge.setSubscribed(true);
        challenge.setDescription("abc");
        challenge.setId(1);
        challenge.setPoints(1);
        assertEquals(true,challenge.isSubscribed());
        assertEquals("abc",challenge.getDescription());
        assertEquals(1,challenge.getId());
        assertEquals(1,challenge.getPoints());
    }

}
