package com.jojonomic.absenin.ActivityTest;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.SpannedString;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.jojonomic.absenin.Absenin;
import com.jojonomic.absenin.BuildConfig;
import com.jojonomic.absenin.LoginActivity;
import com.jojonomic.absenin.SplashScreen;
import com.jojonomic.absenin.R;
import com.jojonomic.absenin.entity.User;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.FileOutputStream;
import java.lang.reflect.Method;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ANGGA on 4/15/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SplashScreenTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Mock
    SplashScreen activity;

    @Before
    public void setUpActivity(){
        activity = Robolectric.buildActivity(SplashScreen.class).create().resume().get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull(activity);
    }

}
