package com.jojonomic.absenin.ActivityTest;

import android.content.Context;
import android.content.SharedPreferences;

import com.jojonomic.absenin.BuildConfig;
import com.jojonomic.absenin.SplashScreen;
import com.jojonomic.absenin.MyLogActivity;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboSharedPreferences;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.ShadowOpenGLMatrix;
import org.robolectric.shadows.ShadowPreference;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by ANGGA on 4/15/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MyLogActivityTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Mock
    MyLogActivity activity;

    public static final String MyPREFERENCES = "MyPrefs" ;

    @Before
    public void setUpActivity(){
        RoboSharedPreferences preferences = (RoboSharedPreferences) RuntimeEnvironment.application
                .getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("userId",1);
        editor.commit();
        activity = Robolectric.buildActivity(MyLogActivity.class).create().resume().get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull(activity);
    }

    @Test
    public void testGetMonthFromString(){
        assertEquals("04",activity.getMonthFromString("2017-04-22 12:50:00"));
        assertEquals("12",activity.getMonthFromString("2017-12-22 12:50:00"));
    }

    @Test
    public void testGetWorkHour(){
        assertEquals("0",activity.getWorkHour("2017-04-22 02:50:00","2017-04-22 02:50:00"));
        assertEquals("10",activity.getWorkHour("2017-12-22 02:50:00","2017-04-22 03:00:00"));
        assertEquals("80",activity.getWorkHour("2017-12-22 02:30:00","2017-04-22 03:50:00"));
    }

}
