package com.jojonomic.absenin.ActivityTest;

import com.jojonomic.absenin.BuildConfig;
import com.jojonomic.absenin.ChallengeListActivity;
import com.jojonomic.absenin.MyProfileActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by ANGGA on 4/15/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MyProfileActivityTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();
    @Mock
    MyProfileActivity activity;

    @Before
    public void setUpActivity(){
        activity = Robolectric.buildActivity(MyProfileActivity.class).create().resume().get();
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull(activity);
    }

}
