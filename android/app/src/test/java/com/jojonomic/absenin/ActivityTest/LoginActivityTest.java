package com.jojonomic.absenin.ActivityTest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.SpannedString;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.jojonomic.absenin.Absenin;
import com.jojonomic.absenin.BuildConfig;
import com.jojonomic.absenin.LoginActivity;
import com.jojonomic.absenin.MainActivity;
import com.jojonomic.absenin.R;
import com.jojonomic.absenin.entity.User;

import org.apache.http.client.methods.HttpPost;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowLog;
import org.robolectric.shadows.httpclient.FakeHttp;
import org.robolectric.shadows.httpclient.TestHttpResponse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by ANGGA on 4/15/2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class LoginActivityTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private final String MyPREFERENCES = "MyPrefs";

    private LoginActivity activity;
    @Mock
    AutoCompleteTextView emailView;
    @Mock
    EditText passwordView;
    @Mock
    Button mEmailSignInButton;
    @Mock
    SharedPreferences sharedPreferences;
    @Mock
    RequestQueue requestQueue;

    @Before
    public void setUpActivity(){
        activity = Robolectric.buildActivity(LoginActivity.class).create().resume().get();
        emailView = (AutoCompleteTextView) activity.findViewById(R.id.email);
        passwordView = (EditText) activity.findViewById(R.id.password);
        mEmailSignInButton = (Button) activity.findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.attemptLogin();
            }
        });
        sharedPreferences = activity.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }

    @Test
    public void shouldNotBeNull() throws Exception
    {
        assertNotNull(activity);
    }

    @Test
    public void checkLoggedInTest(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("loggedIn",true);
        editor.commit();
        activity.checkIfLoggedIn();
        Intent expectedIntent = new Intent(activity, MainActivity.class);
        ShadowActivity shadowActivity = Shadows.shadowOf(activity);
        Intent actualIntent = shadowActivity.getNextStartedActivity();
        assertTrue(actualIntent.filterEquals(expectedIntent));
    }

    @Test
    public void checkViewObjects(){
        assertNotNull(activity.findViewById(R.id.email));
        assertNotNull(activity.findViewById(R.id.password));
    }

    @Test
    public void testPassword(){
        assertEquals(false,activity.isPasswordValid("123"));
    }

    @Test
    public void testLogin(){
        emailView.setText("anggach");
        passwordView.setText("ittech14");
        activity.attemptLogin();
    }

    @Test
    public void testRegister(){
        activity.registerToServer(new User("anggadwiarifandi96@gmail.com","Angga Dwi Arifandi","http://www.google.com",false));
    }

}
