package com.jojonomic.absenin;

import android.content.Context;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.FileOutputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ANGGA on 4/15/2017.
 */

public class AbseninTest extends Absenin{

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    Absenin absenin;

    @Test
    public void checkFlurryApi(){
        absenin = new AbseninTest();
        assertEquals(absenin.getFlurryApiKey(),"X5FJGHVPST7XTGBCXYTW");
    }

}
