package com.jojonomic.absenin;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.app.Application;
import android.support.test.runner.AndroidJUnitRunner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Created by ANGGA on 4/14/2017.
 */

public class AbseninTest extends AndroidJUnitRunner{

    @Override
    public Application newApplication(ClassLoader cl, String className, Context context)
            throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return super.newApplication(cl, Absenin.class.getName(), context);
    }
}
