package com.jojonomic.absenin.entity;

/**
 * Created by ANGGA on 2/19/2017.
 */

public class Lap {
    private String id, lap;

    public Lap() {
    }

    public Lap(String id, String lap){
        this.id=id;
        this.lap=lap;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLap() {
        return lap;
    }

    public void setLap(String lap) {
        this.lap = lap;
    }
}