package com.jojonomic.absenin;

import android.app.Application;

import com.flurry.android.FlurryAgent;

/**
 * Created by ANGGA on 4/11/2017.
 */

public class Absenin extends Application {

    private static final String FLURRY_API_KEY = "X5FJGHVPST7XTGBCXYTW";

    @Override
    public void onCreate() {
        super.onCreate();
        new FlurryAgent.Builder()
                .withLogEnabled(false)
                .build(this, FLURRY_API_KEY);
    }

    public String getFlurryApiKey(){
        return this.FLURRY_API_KEY;
    }
}
