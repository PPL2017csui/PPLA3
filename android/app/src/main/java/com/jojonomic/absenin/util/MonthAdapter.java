package com.jojonomic.absenin.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.jojonomic.absenin.R;
import com.jojonomic.absenin.entity.Lap;
import com.jojonomic.absenin.entity.Month;

import java.util.List;

/**
 * Created by ANGGA on 3/9/2017.
 */

public class MonthAdapter extends ExpandableRecyclerAdapter<Month, Lap, MonthViewHolder, LapViewHolder> {

    private LayoutInflater mInflater;
    private List<Month> mMonthList;

    public MonthAdapter(Context context, @NonNull List<Month> monthList){
        super(monthList);
        mMonthList = monthList;
        mInflater = LayoutInflater.from(context);
    }

    @UiThread
    @NonNull
    @Override
    public MonthViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View recipeView;
        recipeView = mInflater.inflate(R.layout.recipe_view, parentViewGroup, false);
        return new MonthViewHolder(recipeView);
    }

    @UiThread
    @NonNull
    @Override
    public LapViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View ingredientView;
        ingredientView = mInflater.inflate(R.layout.ingredient_view, childViewGroup, false);
        return new LapViewHolder(ingredientView);
    }

    @UiThread
    @Override
    public void onBindParentViewHolder(@NonNull MonthViewHolder recipeViewHolder, int parentPosition, @NonNull Month recipe) {
        recipeViewHolder.bind(recipe);
    }

    @UiThread
    @Override
    public void onBindChildViewHolder(@NonNull LapViewHolder ingredientViewHolder, int parentPosition, int childPosition, @NonNull Lap ingredient) {
        ingredientViewHolder.bind(ingredient);
    }

    @Override
    public int getParentViewType(int parentPosition) {
        if (mMonthList.get(parentPosition).isVegetarian()) {
            return PARENT_VEGETARIAN;
        } else {
            return PARENT_NORMAL;
        }
    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        Lap ingredient = mMonthList.get(parentPosition).getIngredient(childPosition);
        if (ingredient.isVegetarian()) {
            return CHILD_VEGETARIAN;
        } else {
            return CHILD_NORMAL;
        }
    }

    @Override
    public boolean isParentViewType(int viewType) {
        return viewType == PARENT_VEGETARIAN || viewType == PARENT_NORMAL;
    }

}
