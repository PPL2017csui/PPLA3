package com.jojonomic.absenin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jojonomic.absenin.entity.Attendance;
import com.jojonomic.absenin.util.ExpandableListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ANGGA on 3/9/2017.
 */

public class MyLogActivity extends AppCompatActivity {

    public static final String TAG = MyLogActivity.class
            .getSimpleName();
    public static final String MyPREFERENCES = "MyPrefs" ;
    private final int MONTH_YEAR=12;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    private RequestQueue mRequestQueue;
    private ProgressDialog pDialog;
    private int userId;
    // json array response url
    private String urlJsonArry = "http://absen-in.herokuapp.com/api/histories/myhistory";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_log);

        // Always cast your custom Toolbar here, and set it as the ActionBar.
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        // Get the ActionBar here to configure the way it behaves.
        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayHomeAsUpEnabled(true);

        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userId = sharedPreferences.getInt("userId",0);

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.recycler_view);

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // preparing list data
        prepareListData();

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                Toast.makeText(
                        getApplicationContext(),
                                listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
    }

    public String getDateFromString(String time_checkin){
        return time_checkin.substring(8,10);
    }

    public String getMonthFromString(String time_checkin){
        return time_checkin.substring(5,7);
    }

    public String parseWorkHour(String time){
        return time.substring(11,time.length());
    }

    public String getWorkHour(String time_checkin, String time_checkout){
        String time_checkin_hour = time_checkin.substring(11,time_checkin.length());
        String time_checkout_hour = time_checkout.substring(11,time_checkout.length());
        return calculateHour(time_checkin_hour,time_checkout_hour)+"";
    }

    public int calculateHour(String hour1, String hour2){
        int hour1_hour = Integer.parseInt(hour1.substring(0,2));
        int hour1_minute = Integer.parseInt(hour1.substring(3,5));
        int hour2_hour = Integer.parseInt(hour2.substring(0,2));
        int hour2_minute = Integer.parseInt(hour2.substring(3,5));
        if(hour2_minute < hour1_minute){
            return hour2_minute + ( ((hour2_hour-hour1_hour)*60)-hour1_minute );
        }else{
            return (hour2_minute-hour1_minute)+((hour2_hour-hour1_hour)*60);
        }
    }

    public String convertIntToMonth(String i){
        switch (i){
            case "01":
                return "Januari";
            case "02":
                return "Februari";
            case "03":
                return "Maret";
            case "04":
                return "April";
            case "05":
                return "Mei";
            case "06":
                return "Juni";
            case "07":
                return "Juli";
            case "08":
                return "Agustus";
            case "09":
                return "September";
            case "10":
                return "Oktober";
            case "11":
                return "November";
            case "12":
                return "Desember";
            default:
                return "Januari";
        }
    }

    public void getHistoryJSONFromServer(){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                urlJsonArry, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);

                try {
                    // Parsing json object response
                    // response will be a json array
                    JSONArray attendanceYearArray = new JSONArray(response);
                    JSONArray attendanceMonthArray;
                    JSONObject attendance;
                    String time_checkin;
                    String time_checkout;
                    String month;
                    String date;
                    ArrayList<String> attendanceItem;
                    for(int i=0; i<MONTH_YEAR; i++){
                        attendanceMonthArray = attendanceYearArray.getJSONArray(i);
                        attendanceItem = new ArrayList<String>();
                        if(attendanceMonthArray.length() > 0){
                            listDataHeader.add(i+1+"");
                        }
                        for(int j=0; j<attendanceMonthArray.length(); j++){
                            attendance = attendanceMonthArray.getJSONObject(j);
                            time_checkin = attendance.getString("time_checkin");
                            time_checkout = attendance.getString("time_checkout");
                            month = convertIntToMonth(getMonthFromString(time_checkin));
                            date = getDateFromString(time_checkin);
                            attendanceItem.add(date+" "+month+", Checkin : "+parseWorkHour(time_checkin)+" Checkout : "+parseWorkHour(time_checkout));
                        }
                        if(listDataHeader.contains(i+1+""))
                            listDataChild.put(listDataHeader.get(listDataHeader.indexOf(i+1+"")), attendanceItem);
                    }
                    listAdapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.toString());
                Toast.makeText(getApplicationContext(),
                        error.toString(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userId+"");
                return params;
            }
        };

        // Adding request to request queue
        mRequestQueue = Volley.newRequestQueue(MyLogActivity.this);
        mRequestQueue.add(stringRequest);
    }

    private void prepareListData() {
        Log.d(TAG,"Start Refreshing Data");
        listDataHeader.clear();
        listDataChild.clear();

        getHistoryJSONFromServer();

        Log.d(TAG,"Done Refreshing Data");
    }

    @Override
   protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

   @Override
   protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
       super.onRestoreInstanceState(savedInstanceState);
   }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
