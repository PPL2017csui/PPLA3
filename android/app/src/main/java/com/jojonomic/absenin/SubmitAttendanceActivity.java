package com.jojonomic.absenin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.identity.intents.Address;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ANGGA on 3/20/2017.
 */

public class SubmitAttendanceActivity extends AppCompatActivity implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static final String TAG = SubmitAttendanceActivity.class
            .getSimpleName();
    TextView currentLocationView;

    public static final String MyPREFERENCES = "MyPrefs" ;

    // GoogleMap Variables
    private GoogleMap map;
    protected SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private boolean locationDetected;
    private RequestQueue mRequestQueue;
    private ProgressDialog pDialog;
    private Location currentLocation;
    private SharedPreferences sharedPreferences;
    private int userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_attendance);

        // Always cast your custom Toolbar here, and set it as the ActionBar.
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        // Get the ActionBar here to configure the way it behaves.
        final ActionBar ab = getSupportActionBar(); // get actionbar object for further edits
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayHomeAsUpEnabled(true);

        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        userId = sharedPreferences.getInt("userId",0);

        this.locationDetected = false;

        initializeMap();

        currentLocationView = (TextView) findViewById(R.id.current_location);

        final EditText desc = (EditText) findViewById(R.id.desc_box);
        
        Button submitButton = (Button) findViewById(R.id.submit_button);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = desc.getText().toString();
                double longitude = currentLocation.getLongitude();
                double latitude = currentLocation.getLatitude();
                submitAttendanceToServer(userId,longitude,latitude,description);
            }
        });
    }

    public boolean checkDistance(double latitude, double longitude){
        double distance = SphericalUtil.computeDistanceBetween(new LatLng(latitude,longitude),new LatLng(-6.364601,106.828689));
        Log.d(TAG,"distance "+distance);
        // check location if in bounds with office
        if(distance > 1000){
            Log.d(TAG,"distance bigger than 1000");
            AlertDialog.Builder builder = new AlertDialog.Builder(SubmitAttendanceActivity.this);
            builder.setCancelable(false);
            builder.setMessage("CANNOT SUBMIT ATTENDANCE, YOU'RE NOT IN THE OFFICE AREA");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
            return false;
        }
        return true;
    }

    public void submitAttendanceToServer(final int userId, final double longitude, final double latitude, final String description) {

        //if(!checkDistance(latitude, longitude))
           // return;

        String url = "http://absen-in.herokuapp.com/api/histories/submit";

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        if(!pDialog.isShowing())
            pDialog.show();

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.d("DEBUGZ", response.toString());
                        if(response.toString().equalsIgnoreCase("error")){
                            Log.d(TAG, "Error: " + response.toString());
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + response.toString(),
                                    Toast.LENGTH_LONG).show();
                            pDialog.hide();
                        }else{
                            Toast.makeText(getApplicationContext(),
                                    "SUCCESS SUBMIT DATA",
                                    Toast.LENGTH_LONG).show();
                            try{
                                JSONObject jsonObject = new JSONObject(response);
                                int currentId = jsonObject.getInt("id");
                                SharedPreferences sharedPreferences1 = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences1.edit();
                                editor.putBoolean("hasAttend",true);
                                editor.putInt("currentAttendanceId",currentId);
                                editor.commit();
                                pDialog.hide();
                                finish();
                            }catch (JSONException e){
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(),
                                        e.toString(),
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null || error.toString().length() > 0) {
                    Log.d(TAG, "Error: " + error.getMessage());
                    Toast.makeText(getApplicationContext(),
                            "Error: " + error.getMessage(),
                            Toast.LENGTH_LONG).show();
                    pDialog.hide();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", userId+"");
                params.put("long", longitude+"");
                params.put("lat", latitude+"");
                params.put("description", description);

                return params;
            }
        };

        // Adding request to request queue
        // Adding request to request queue
        mRequestQueue = Volley.newRequestQueue(SubmitAttendanceActivity.this);
        mRequestQueue.add(jsonObjReq);

    }

    private void initializeMap() {
        if (map == null) {
            SupportMapFragment mapFragment =
                    (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        this.map.setMyLocationEnabled(true);
        buildGoogleApiClient();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(locationDetected)
            return;
        currentLocation = location;
        Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
        currentLocationView.setText("My Location");
        try {
            String address = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0).getThoroughfare();
            currentLocationView.setText(address);
            map.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title(address));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12));
            locationDetected = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(this, "ERROR GETTING ADDRESS", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //TODO
    }

    @Override
    public void onConnectionSuspended(int i) {
        //TODO
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100000000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }
}