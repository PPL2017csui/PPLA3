package com.jojonomic.absenin.util;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jojonomic.absenin.R;
import com.jojonomic.absenin.entity.Lap;

import java.util.List;

/**
 * Created by ANGGA on 2/19/2017.
 */

public class LapAdapter extends RecyclerView.Adapter<LapAdapter.MyViewHolder> {

    private List<Lap> lapList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            genre = (TextView) view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
        }
    }

    public LapAdapter(List<Lap> lapList) {
        this.lapList = lapList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lap_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Lap lap = lapList.get(position);
        holder.title.setText(lap.getId());
        holder.year.setText(lap.getLap().substring(11));
    }

    @Override
    public int getItemCount() {
        return lapList.size();
    }
}

