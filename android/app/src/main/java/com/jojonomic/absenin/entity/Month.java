package com.jojonomic.absenin.entity;

/**
 * Created by ANGGA on 3/9/2017.
 */

public class Month  {

    private String name;

    public Month() {}

    public Month(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }
}
