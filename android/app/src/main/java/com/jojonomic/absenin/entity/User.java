package com.jojonomic.absenin.entity;

/**
 * Created by ANGGA on 3/29/2017.
 */

public class User {
    private String email;
    private String name;
    private String photoUrl;
    private boolean hasAttend;

    public User(){
    }

    public User(String email, String name, String photoUrl, boolean hasAttend) {
        this.email = email;
        this.name = name;
        this.photoUrl = photoUrl;
        this.hasAttend = hasAttend;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean hasAttend() {
        return hasAttend;
    }

    public void setHasAttend(boolean hasAttend) {
        this.hasAttend = hasAttend;
    }
}
