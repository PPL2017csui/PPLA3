package com.jojonomic.absenin.entity;

/**
 * Created by ANGGA on 4/24/2017.
 */

public class Challenge {

    private String description, rules;
    private int points, id;
    private boolean subscribed;

    public Challenge(){

    }

    public Challenge(String description, String rules, int points, int id) {
        this.description = description;
        this.rules = rules;
        this.points = points;
        this.id = id;
        this.subscribed = false;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public int getId(){return this.id;}

    public void setId(int id){this.id=id;}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
