package com.jojonomic.absenin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jojonomic.absenin.entity.Challenge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ANGGA on 4/15/2017.
 */

public class MyProfileActivity extends AppCompatActivity {

    private static final String TAG = MyProfileActivity.class.getSimpleName();
    public static final String MyPREFERENCES = "MyPrefs" ;
    private ProgressDialog pDialog;
    private RequestQueue mRequestQueue;
    private int userId;
    private EditText name,phone;
    private TextView email , birthdate, points;
    private Button saveButton;
    private String urlGetProfile = "http://absen-in.herokuapp.com/api/user/getProfile";
    private String urlSetProfile = "http://absen-in.herokuapp.com/api/user/setProfile";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        // Always cast your custom Toolbar here, and set it as the ActionBar.
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        // Get the ActionBar here to configure the way it behaves.
        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayHomeAsUpEnabled(true);

        getSharedUserId();

        collectLayoutItems();

        fetchData();

    }

    public void collectLayoutItems(){
        name = (EditText) findViewById(R.id.name);
        email = (TextView) findViewById(R.id.email);
        birthdate = (TextView) findViewById(R.id.birth_date);
        phone = (EditText) findViewById(R.id.phone);
        points = (TextView) findViewById(R.id.points);
        saveButton = (Button) findViewById(R.id.save_button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
    }

    public void getSharedUserId(){
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userId = sharedPreferences.getInt("userId",0);
    }

    public void saveData(){

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        mRequestQueue = getRequestQueue();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                urlSetProfile, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "for id "+userId+" "+response);

                try {

                    JSONObject userJson = new JSONObject(response);
                    name.setText(userJson.getString("name"));
                    phone.setText(userJson.getString("phone"));
                }catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(MyProfileActivity.this, "EXCEPTION "+e.toString(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }finally {
                    pDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.toString());
                Toast.makeText(getApplicationContext(),
                        error.toString(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", userId+"");
                params.put("name", name.getText().toString());
                params.put("phone", phone.getText().toString());
                return params;
            }
        };

        // Adding request to request queue
        mRequestQueue = Volley.newRequestQueue(MyProfileActivity.this);
        mRequestQueue.add(stringRequest);

    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public void fetchData(){

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        mRequestQueue = getRequestQueue();

        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                urlGetProfile, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "for id "+userId+" "+response);

                try {
                    JSONObject responseJson = new JSONObject(response);
                    name.setText(responseJson.getString("name"));
                    email.setText(responseJson.getString("email"));
                    if(!responseJson.getString("birth_date").equalsIgnoreCase("null"))
                        birthdate.setText(responseJson.getString("birth_date"));
                    if(!responseJson.getString("phone").equalsIgnoreCase("null"))
                        phone.setText(responseJson.getString("phone"));
                    points.setText(responseJson.getString("points"));
                }catch (JSONException e){
                    e.printStackTrace();
                    Toast.makeText(MyProfileActivity.this, "EXCEPTION "+e.toString(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }

                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.toString());
                Toast.makeText(getApplicationContext(),
                        error.toString(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id", userId+"");
                return params;
            }
        };

        // Adding request to request queue
        mRequestQueue = Volley.newRequestQueue(MyProfileActivity.this);
        mRequestQueue.add(stringRequest);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
