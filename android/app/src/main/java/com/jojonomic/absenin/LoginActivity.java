package com.jojonomic.absenin;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jojonomic.absenin.entity.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.Manifest.permission.READ_CONTACTS;
import static android.accounts.AccountManager.KEY_PASSWORD;


/**
 * Created by ANGGA on 3/20/2017.
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity{

    public static final String TAG = LoginActivity.class
            .getSimpleName();

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;

    public static final String MyPREFERENCES = "MyPrefs";
    private ProgressDialog progressDialog;
    private RequestQueue mRequestQueue;
    private SharedPreferences sharedPreferences;
    private String image_link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkIfLoggedIn();

        setUpViewObjects();
    }

    public void setUpViewObjects(){
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
    }

    public void checkIfLoggedIn(){
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Log.d(TAG,"Checking if logged in");

        if(!sharedPreferences.contains("loggedIn")) {
            Log.d(TAG, "Not logged in yet");
            editor.putBoolean("loggedIn", false);
        }else if(sharedPreferences.getBoolean("loggedIn",false)){
            Log.d(TAG,"Already logged in");
            Intent i = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(i);
            finish();
        }
        editor.commit();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (password.isEmpty() || !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            Log.d(TAG,"login cancelled");
            focusView.requestFocus();
        } else {

            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
           // showProgress(true);
            Log.d(TAG,"login process started");
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            String url = "https://api.jojonomic.com/app/api/v2/pro/user/login";

            Map<String, Object> jsonParams = new HashMap<>();
            jsonParams.put("email",email);
            jsonParams.put("password",password);
            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(jsonParams),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                boolean error = response.getBoolean("error");
                                Log.d("DEBUGZ","APAKAH ERROR "+error);
                                Log.d("DEBUGZ",response.toString());
                                if(error){
                                    Toast.makeText(LoginActivity.this, "CREDENTIALS INVALID!", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                JSONObject userObject = response.getJSONObject("user");
                                String email = userObject.getString("email");
                                String name = userObject.getString("first_name")+" "+userObject.getString("last_name");
                                String photo_url = userObject.getString("photo_url");
                                image_link = photo_url;
                                Log.d("DEBUGZ",photo_url);
                                User user = new User(email,name,photo_url,false);
                                registerToServer(user);
                            }catch (JSONException exception){
                                Log.d(TAG, exception.toString());
                                Toast.makeText(LoginActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if( error instanceof NetworkError) {
                                Log.d("DEBUGZ","NETWORK ERROR");
                                Toast.makeText(LoginActivity.this, "NO INTERNET CONNECTION DETECTED!", Toast.LENGTH_LONG).show();
                            } else if( error instanceof ServerError) {
                                Log.d("DEBUGZ","SERVER ERROR");
                                Toast.makeText(LoginActivity.this, "WRONG USERNAME OR PASSWORD!", Toast.LENGTH_LONG).show();
                            } else if( error instanceof AuthFailureError) {
                                Log.d("DEBUGZ","AUTH FAILURE ERROR");
                                Toast.makeText(LoginActivity.this, "WRONG USERNAME OR PASSWORD!", Toast.LENGTH_LONG).show();
                            } else if( error instanceof ParseError) {
                                Log.d("DEBUGZ","PARSE ERROR");
                            } else if( error instanceof NoConnectionError) {
                                Log.d("DEBUGZ","NO CONNECTION ERROR");
                                Toast.makeText(LoginActivity.this, "NO INTERNET CONNECTION DETECTED!", Toast.LENGTH_LONG).show();
                            } else if( error instanceof TimeoutError) {
                                Log.d("DEBUGZ","TIMEOUT ERROR");
                                Toast.makeText(LoginActivity.this, "INTERNET CONNECTION TIMEOUT DETECTED!", Toast.LENGTH_LONG).show();
                            }
                            progressDialog.dismiss();
                        }
                    }
            );

            mRequestQueue = Volley.newRequestQueue(LoginActivity.this);
            mRequestQueue.add(stringRequest);

        }
    }

    public boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    public boolean registerToServer(final User user){
            try {
                String url = "http://absen-in.herokuapp.com/api/user/login";

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try{
                                    JSONArray jsonArray = new JSONArray(response);
                                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                                    int userID = jsonObject.getInt("id");
                                    String name = jsonObject.getString("name");
                                    String email = jsonObject.getString("email");
                                    SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
                                    editor.putBoolean("loggedIn",true);
                                    editor.putInt("userId",userID);
                                    editor.putString("name",name);
                                    editor.putString("email",email);
                                    editor.putString("image_url",image_link);
                                    editor.commit();
                                    Intent i = new Intent(LoginActivity.this,MainActivity.class);
                                    startActivity(i);
                                    progressDialog.dismiss();
                                    finish();
                                }catch (JSONException exception){
                                    Log.d(TAG, exception.toString());
                                    Toast.makeText(LoginActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if(error == null || error.getLocalizedMessage() == null)
                                    return;
                                if(error.getLocalizedMessage().contains("java.net.UnknownHostException"))
                                    Toast.makeText(LoginActivity.this, "No Internet Connection!", Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(LoginActivity.this, error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("email", user.getEmail());
                        params.put("name", user.getName());
                        return params;
                    }
                };

                mRequestQueue = Volley.newRequestQueue(LoginActivity.this);
                mRequestQueue.add(stringRequest);
            } catch (Exception e) {
                return false;
            }
            return false;
        }
}

