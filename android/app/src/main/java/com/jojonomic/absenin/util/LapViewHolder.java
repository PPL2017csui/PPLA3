package com.jojonomic.absenin.util;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.jojonomic.absenin.entity.Lap;

/**
 * Created by ANGGA on 3/9/2017.
 */

public class LapViewHolder extends ChildViewHolder{

    private TextView mIngredientTextView;

    public IngredientViewHolder(@NonNull View itemView) {
        super(itemView);
        mIngredientTextView = (TextView) itemView.findViewById(R.id.ingredient_textview);
    }

    public void bind(@NonNull Lap lap) {
        mIngredientTextView.setText(lap.getId());
    }

}
