package com.jojonomic.absenin;

/**
 * Created by ANGGA on 3/8/2017.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jojonomic.absenin.entity.Attendance;
import com.jojonomic.absenin.util.CircleTransform;
import com.jojonomic.absenin.util.AttendanceAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ANGGA on 3/20/2017.
 * A login screen that offers login via email/password.
 */
public class MainActivity extends AppCompatActivity implements GoogleMap.OnMarkerClickListener, LocationListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = MainActivity.class
            .getSimpleName();

    public static final String MyPREFERENCES = "MyPrefs" ;
    private final int FLAG_ATTENDANCE = 1;
    private final int FLAG_LEAVE = 2;

    // temporary string to show the parsed response
    private String jsonResponse;
    private RequestQueue mRequestQueue;

    // View Variables
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite, addAttendanceText;
    private ProgressDialog pDialog;
    private String name;
    private String image_url;
    private int currentId;

    // GoogleMap Variables
    private GoogleMap map;
    protected SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    // Attendance List Variables
    private List<Attendance> attendanceList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AttendanceAdapter mAdapter;
    private int refreshFlag;
    private boolean locationDetected;

    private SharedPreferences sharedPreferences;

    // urls to load navigation header background image
    // and profile image
    private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private static final String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";
    // json array response url
    private String urlJsonArry = "http://absen-in.herokuapp.com/api/histories";

    /**
     *
     * override activity methods
     */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        refreshFlag = FLAG_ATTENDANCE;
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        name = sharedPreferences.getString("name","");
        image_url = sharedPreferences.getString("image_url","");
        currentId = sharedPreferences.getInt("currentAttendanceId",0);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);

        this.locationDetected = false;

        // Always cast your custom Toolbar here, and set it as the ActionBar.
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        // Get the ActionBar here to configure the way it behaves.
        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout

        // load nav menu header data
        loadNavHeader();

        addFunctionToLayoutObjects();

        addAttendanceText = (TextView) findViewById(R.id.add_attendance_button_text);

        if(sharedPreferences.getBoolean("hasAttend",false)){
            addAttendanceText.setText("CHECKOUT");
        }

        initializeMap();

        // setup recycler view
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new AttendanceAdapter(attendanceList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        refreshData(refreshFlag);
    }

    private void addFunctionToLayoutObjects(){

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_settings:
                        Toast.makeText(MainActivity.this, "settings clicked",Toast.LENGTH_LONG).show();
                        break;
                    case R.id.nav_about_us:
                        Intent k = new Intent(MainActivity.this,AboutActivity.class);
                        startActivity(k);
                        return true;
                    case R.id.nav_challenge_list:
                        Intent i = new Intent(MainActivity.this,ChallengeListActivity.class);
                        startActivity(i);
                        return true;
                    case R.id.nav_my_profile:
                        Intent j = new Intent(MainActivity.this,MyProfileActivity.class);
                        startActivity(j);
                        return true;
                    case R.id.nav_logout:
                        logout();
                        return true;
                    default:
                        return true;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                return true;
            }
        });

        final LinearLayout middleLayout = (LinearLayout)  findViewById(R.id.middlelayout);
        LinearLayout middleButton = (LinearLayout) findViewById(R.id.middlebutton);
        middleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(middleLayout.getVisibility() == View.VISIBLE){
                    middleLayout.setVisibility(View.GONE);
                    ((ImageView) findViewById(R.id.arrowimg)).setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                }
                else{
                    middleLayout.setVisibility(View.VISIBLE);
                    ((ImageView) findViewById(R.id.arrowimg)).setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }
            }
        });
        final LinearLayout attendanceBox = (LinearLayout) findViewById(R.id.attendancebox);
        final LinearLayout leaveBox = (LinearLayout) findViewById(R.id.leavebox);
        attendanceBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(attendanceBox.getBackground() == null){
                    attendanceBox.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.border, null));
                    leaveBox.setBackground(null);
                    refreshFlag = FLAG_ATTENDANCE;
                    refreshData(refreshFlag);
                }
            }
        });

        leaveBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(leaveBox.getBackground() == null){
                    leaveBox.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.border, null));
                    attendanceBox.setBackground(null);
                    refreshFlag = FLAG_LEAVE;
                    refreshData(refreshFlag);
                }
            }
        });

        LinearLayout myLogButton = (LinearLayout) findViewById(R.id.mylogsbutton);
        myLogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, MyLogActivity.class);
                startActivity(i);
            }
        });

        LinearLayout submitAttendanceButton = (LinearLayout) findViewById(R.id.submitattendancebutton);
        submitAttendanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences1 = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                if(sharedPreferences1.getBoolean("hasAttend",false)){
                    currentId = sharedPreferences1.getInt("currentAttendanceId",0);
                    checkOut(currentId);
                }else{
                    // SUBMIT ATTENDANCE
                    Intent i = new Intent(MainActivity.this, SubmitAttendanceActivity.class);
                    startActivity(i);
                }
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        SharedPreferences sharedPreferences1 = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        if(sharedPreferences1.getBoolean("hasAttend",false)){
            addAttendanceText.setText("CHECKOUT");
        }else{
            addAttendanceText.setText("ADD ATTENDANCE");
        }
        refreshData(refreshFlag);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_logout:
                logout();
                return true;
            case R.id.action_about:
                Intent k = new Intent(MainActivity.this,AboutActivity.class);
                startActivity(k);
                return true;
            case R.id.action_refresh:
                refreshData(refreshFlag);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * end of overridden activity method group
     */

    /**
     * custom methods for API and stuff
     */

    private void loadNavHeader() {
        // name, website
        txtName.setText(name);

        loadHeaderImage();

        loadProfileImage();
    }

    private void loadHeaderImage(){
        // loading header background image
        Glide.with(this).load(urlNavHeaderBg)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);
    }

    private void loadProfileImage(){
        // Loading profile image
        Glide.with(this).load(image_url)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);
    }

    public void checkOut(final int currentId){

            String url = "http://absen-in.herokuapp.com/api/histories/checkout";

            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            if(!pDialog.isShowing())
                pDialog.show();

            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                    url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
                            editor.putBoolean("hasAttend",false);
                            editor.remove("currentAttendanceId");
                            editor.commit();
                            addAttendanceText.setText("ADD ATTENDANCE");
                            pDialog.dismiss();
                            Toast.makeText(MainActivity.this, "SUCCESSFULLY CHECKED OUT", Toast.LENGTH_SHORT).show();
                            refreshData(refreshFlag);
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null || error.toString().length() > 0) {
                        Log.d(TAG, "Error: " + error.toString());
                        Toast.makeText(getApplicationContext(),
                                "Error: " + error.toString(),
                                Toast.LENGTH_LONG).show();
                        pDialog.dismiss();
                    }
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("id", currentId+"");

                    return params;
                }
            };


            // Adding request to request queue
            mRequestQueue = Volley.newRequestQueue(MainActivity.this);
            mRequestQueue.add(jsonObjReq);
    }

    public void logout(){
        Toast.makeText(MainActivity.this, "Logging you out",Toast.LENGTH_LONG).show();
        SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();
        editor.putBoolean("loggedIn",false);
        editor.commit();
        Log.d(TAG,"Logging out");
        Intent i = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(i);
        finish();
    }

    public void getHistoryJSONFromServer(final int refreshFlag){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(Request.Method.GET,
                urlJsonArry, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    for (int i = 0; i < response.length(); i++) {

                        JSONObject attendance = (JSONObject) response.get(i);

                        String name = attendance.getString("name");
                        String time_checkin = attendance.getString("time_checkin");
                        String time_checkout = attendance.getString("time_checkout");

                        if(refreshFlag == FLAG_ATTENDANCE && time_checkout.equalsIgnoreCase("null")){
                            attendanceList.add(new Attendance(name,time_checkin));
                        }
                        if(refreshFlag == FLAG_LEAVE && !time_checkout.equalsIgnoreCase("null")){
                            attendanceList.add(new Attendance(name,time_checkin));
                        }

                    }
                    mAdapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.toString());
                Toast.makeText(getApplicationContext(),
                        error.toString(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.dismiss();
            }
        });

        // Adding request to request queue
        mRequestQueue = Volley.newRequestQueue(MainActivity.this);
        mRequestQueue.add(jsonArrayReq);
    }

    public void refreshData(int refreshFlag){
        Log.d(TAG,"Start Refreshing Data");
        attendanceList.clear();
        mAdapter.notifyDataSetChanged();
        getHistoryJSONFromServer(refreshFlag);
        Log.d(TAG,"Done Refreshing Data");
    }

    /**
     * end of custom methods for API and stuff
     */

    /**
     *
     * Google Maps methods group
     *
     */

    private void initializeMap() {
        if (map == null) {
            SupportMapFragment mapFragment =
                    (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        this.map.setMyLocationEnabled(true);
        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(locationDetected)
            return;
        Geocoder geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
        try {
            String address = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0).getThoroughfare();
            map.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title(address));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12));
            locationDetected = true;
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(this, "ERROR GETTING ADDRESS YOUR LOCATION IS NOT FOUND!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100000000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    /**
     * End of Google Maps methods group
     */
}
