package com.jojonomic.absenin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jojonomic.absenin.entity.Attendance;
import com.jojonomic.absenin.entity.Challenge;
import com.jojonomic.absenin.util.AttendanceAdapter;
import com.jojonomic.absenin.util.ChallengeAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ANGGA on 4/15/2017.
 */

public class ChallengeListActivity extends AppCompatActivity{

    private static final String TAG = ChallengeListActivity.class.getSimpleName();
    public static final String MyPREFERENCES = "MyPrefs" ;
    private ProgressDialog pDialog;
    private RequestQueue mRequestQueue;

    // Attendance List Variables
    private List<Challenge> challengeList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ChallengeAdapter mAdapter;
    private int userId;
    private String urlJsonArry = "http://absen-in.herokuapp.com/api/challenges";
    private String urlJsonArry_subscribe = "http://absen-in.herokuapp.com/api/user_challenge/subscribe_unsubscribe";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_list);

        // Always cast your custom Toolbar here, and set it as the ActionBar.
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tb);

        // Get the ActionBar here to configure the way it behaves.
        final ActionBar ab = getSupportActionBar();
        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayHomeAsUpEnabled(true);

        // setup recycler view
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new ChallengeAdapter(challengeList, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChallengeAdapter.MyViewHolder holder = (ChallengeAdapter.MyViewHolder) v.getTag();
                int pos = holder.getLayoutPosition();
                alertChallenge(challengeList.get(pos),(Button)v);
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        getUserId();
        refreshData();
    }

    public void getUserId(){
        SharedPreferences sharedPreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        userId = sharedPreferences.getInt("userId",0);
    }

    public void alertChallenge(final Challenge challenge, final Button button){
        final AlertDialog.Builder builder = new AlertDialog.Builder(ChallengeListActivity.this);
        String subscribed;
        if(!challenge.isSubscribed())
            subscribed = "subscribe";
        else
            subscribed = "unsubscribe";
        builder.setMessage("Do you wanna "+subscribed+" to this challenge?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!challenge.isSubscribed())
                    subscribeChallenge(challenge, button);
                else
                    unsubscribeChallenge(challenge, button);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    public void subscribeChallenge(final Challenge challenge, final Button button){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        mRequestQueue = getRequestQueue();

        StringRequest jsonObjectReq = new StringRequest(Request.Method.POST,
                urlJsonArry_subscribe, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                if(!response.equalsIgnoreCase("\"success\""))
                    Toast.makeText(ChallengeListActivity.this, "SUBSCRIBE CHALLENGE UNSUCCESSFULL!", Toast.LENGTH_SHORT).show();
                else {
                    Toast.makeText(ChallengeListActivity.this, "SUBSCRIBE CHALLENGE SUCCESSFULL!", Toast.LENGTH_SHORT).show();
                    challenge.setSubscribed(true);
                    button.setText("UNSUBSCRIBE");
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.toString());
                Toast.makeText(getApplicationContext(),
                        error.toString(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("challenge_id", challenge.getId()+"");
                params.put("user_id", userId+"");
                params.put("flag","1");
                return params;
            }
        };

        // Adding request to request queue
        mRequestQueue = Volley.newRequestQueue(ChallengeListActivity.this);
        mRequestQueue.add(jsonObjectReq);
    }

    public void unsubscribeChallenge(final Challenge challenge, final Button button){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        mRequestQueue = getRequestQueue();

        StringRequest jsonObjectReq = new StringRequest(Request.Method.POST,
                urlJsonArry_subscribe, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, response);
                if(!response.equalsIgnoreCase("\"success\""))
                    Toast.makeText(ChallengeListActivity.this, "UNSUBSCRIBE CHALLENGE UNSUCCESSFULL!", Toast.LENGTH_SHORT).show();
                else {
                    Toast.makeText(ChallengeListActivity.this, "UNSUBSCRIBE CHALLENGE SUCCESSFULL!", Toast.LENGTH_SHORT).show();
                    challenge.setSubscribed(false);
                    button.setText("SUBSCRIBE");
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.toString());
                Toast.makeText(getApplicationContext(),
                        error.toString(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("challenge_id", challenge.getId()+"");
                params.put("user_id", userId+"");
                params.put("flag","0");
                return params;
            }
        };

        // Adding request to request queue
        mRequestQueue = Volley.newRequestQueue(ChallengeListActivity.this);
        mRequestQueue.add(jsonObjectReq);
    }

    public void refreshData(){
        Log.d(TAG,"Start Refreshing Data");
        challengeList.clear();
        mAdapter.notifyDataSetChanged();
        getChallengeJSONFromServer();
        Log.d(TAG,"Done Refreshing Data");
    }

    public void getChallengeJSONFromServer(){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        mRequestQueue = getRequestQueue();

        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(Request.Method.GET,
                urlJsonArry, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    JSONObject challenge;
                    String description,rules;
                    int points,id;
                    for (int i = 0; i < response.length(); i++) {

                        challenge = (JSONObject) response.get(i);
                        description = challenge.getString("description");
                        rules = challenge.getString("rules");
                        points = challenge.getInt("points");
                        id = challenge.getInt("id");
                        challengeList.add(new Challenge(description,rules,points,id));
                    }
                    Log.d(TAG,"sekarang size challenge ada "+challengeList.size());
                    mAdapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                pDialog.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.toString());
                Toast.makeText(getApplicationContext(),
                        error.toString(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.dismiss();
            }
        });

        // Adding request to request queue
        mRequestQueue = Volley.newRequestQueue(ChallengeListActivity.this);
        mRequestQueue.add(jsonArrayReq);
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
