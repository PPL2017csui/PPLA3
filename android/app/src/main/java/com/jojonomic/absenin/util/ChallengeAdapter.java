package com.jojonomic.absenin.util;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jojonomic.absenin.R;
import com.jojonomic.absenin.entity.Challenge;

import java.util.List;

/**
 * Created by ANGGA on 4/24/2017.
 *
 */

public class ChallengeAdapter extends RecyclerView.Adapter<ChallengeAdapter.MyViewHolder> {

    private List<Challenge> challengeList;
    private View.OnClickListener onClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView description, rules, points, id;
        private Button subscribeButton;

        public MyViewHolder(View view) {
            super(view);
            description = (TextView) view.findViewById(R.id.description);
            rules = (TextView) view.findViewById(R.id.rules);
            points = (TextView) view.findViewById(R.id.points);
            id = (TextView) view.findViewById(R.id.id);
            subscribeButton = (Button) view.findViewById(R.id.subscribe_button);
        }
    }

    public ChallengeAdapter(List<Challenge> challengeList, View.OnClickListener onClickListener) {
        this.challengeList = challengeList;
        this.onClickListener = onClickListener;
    }

    @Override
    public ChallengeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.challenge_list_row, parent, false);
        return new ChallengeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChallengeAdapter.MyViewHolder holder, int position) {
        Challenge challenge = challengeList.get(position);
        holder.description.setText(challenge.getDescription());
        holder.rules.setText(challenge.getRules());
        holder.points.setText(challenge.getPoints()+"");
        holder.id.setText("Challenge "+challenge.getId());
        holder.subscribeButton.setTag(holder);
        holder.subscribeButton.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return challengeList.size();
    }

}