<?php

namespace App\Console\Commands;

use App\History;
use Illuminate\Console\Command;
use Carbon\Carbon;

class AutoCheckout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkout:auto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'set a default checkout if user not checkout';
    private $histories = null;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::today()->toDateString();
        $this->histories = History::whereNull('time_checkout')->whereDate('created_at','=',$now)->get();
        $this->histories->each(function($history){
                $history->time_checkout = Carbon::now()->toDayDateTimeString();
                $history->save();
        });
    }
}
