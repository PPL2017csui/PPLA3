<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Log_Point extends Model
{
    protected $fillable = ['user_id', 'challenge_name', 'points'];
    protected $table = 'log_points';

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function has($attribute)
    {
        return in_array($attribute, $this->fillable);
    }

    public function getTable()
    {
        return $this->table;
    }
}
