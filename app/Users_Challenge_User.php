<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_Challenge_User extends Model
{
    protected $table = 'users_challenge_users';
    protected $fillable = ['challenger_id', 'challenged_id'];

    public function getTable()
    {
    	return $this->table;
    }

    public function has($attribute)
    {
    	return in_array($attribute, $this->fillable);
    }
}
