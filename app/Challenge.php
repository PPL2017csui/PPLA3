<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challenge extends Model
{
    protected $fillable = [
        'name',
        'description',
        'points',
        'arrival_time',
        'start_valid_date',
        'end_valid_date',
        'is_personal'
    ];
    protected $table = 'challenges';

    public function subscribed()
    {
        return $this->belongsToMany('App\User', 'users_subscribes', 'challenge_id', 'user_id');
    }

    public function challenge()
    {
        return $this->hasMany('App\User_Challenge');
    }

    public function getTable()
    {
        return $this->table;
    }

    public function has($attribute)
    {
        return in_array($attribute, $this->fillable);
    }
}
