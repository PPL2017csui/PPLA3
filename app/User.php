<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = ['name', 'email', 'role', 'birth_date', 'phone', 'points'];
    protected $table = 'users';

    public function history()
    {
        return $this->hasMany('App\History');
    }

    public function log_point()
    {
        return $this->hasMany('App\log_point');
    }

    public function subscribes()
    {
        return $this->belongsToMany('App\Challenge', 'users_subscribes', 'user_id', 'challenge_id');
    }

    public function challenger()
    {
        return $this->hasMany('App\User_Challenge', 'user_id_challenger');
    }

    public function challenged()
    {
        return $this->hasMany('App\User_Challenge', 'user_id_challenged');
    }

    public function has($attribute)
    {
        return in_array($attribute, $this->fillable);
    }

    public function getTable()
    {
        return $this->table;
    }
}
