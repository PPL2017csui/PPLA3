<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Subscribe extends Model
{
    protected $table = 'users_subscribes';
    protected $fillable = ['user_id', 'challenge_id', 'is_subscribed'];

    public function getTable()
    {
        return $this->table;
    }

    public function has($attribute)
    {
        return in_array($attribute, $this->fillable);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function challenge()
    {
        return $this->belongsTo('App\Challenge');
    }
}
