<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Challenge extends Model
{
    protected $table = 'users_challenges';
    protected $fillable = ['user_id_challenger', 'user_id_challenged', 'challenge_id', 'is_subscribed'];

    public function challenger()
    {
        return $this->belongsTo('App\User');
    }

    public function challenged()
    {
        return $this->belongsTo('App\User');
    }

    public function challenge()
    {
        return $this->belongsTo('App\Challenge');
    }

    public function getTable()
    {
        return $this->table;
    }

    public function has($attribute)
    {
        return in_array($attribute, $this->fillable);
    }
}
