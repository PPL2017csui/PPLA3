<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'histories';
    protected $fillable = ['user_id', 'time_checkin', 'time_checkout', 'long', 'lat', 'description'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getTable()
    {
        return $this->table;
    }

    public function has($attribute)
    {
        return in_array($attribute, $this->fillable);
    }
}
