<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Challenge;
use App\User_Challenge;

class User_ChallengeController extends Controller
{
    public function index()
    {
        $subscribers = User_Challenge::all();
        return Response::json($subscribers, 200);
    }
}
