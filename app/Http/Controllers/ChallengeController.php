<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Challenge;
use App\User_Subscribe;
use Carbon\Carbon;
use Redirect;

class ChallengeController extends Controller
{
    public function index()
    {
        $date = Carbon::now()->toDateString();

        $user_id = Input::get('user_id');
        $subscribed_challenges = User_Subscribe::where('user_id', '=', $user_id)->where('is_subscribed', '=', true)->get();
        $names = collect([]);
        if(count($subscribed_challenges) > 1){
            foreach ($subscribed_challenges as $challenge) {
                    $names->push(Challenge::find($challenge->challenge_id)->name);
            }
        }else{
            $names->push(Challenge::find($subscribed_challenges[0]->challenge_id)->name);
        }

        $challenge = Challenge::where('start_valid_date', '<=', $date)->where('end_valid_date', '>=', $date)->whereNotIn('name',$names)->get();

        if ($challenge->isEmpty()) {
            return Response::json("no active challenges", 200);
        }
        return Response::json($challenge, 200);
    }

    public function challenge_view()
    {
        $date = Carbon::now()->toDateString();
        $challenges_view = Challenge::where('start_valid_date', '<=', $date)
            ->where('end_valid_date', '>=', $date)
            ->whereNotNull('start_valid_date')
            ->whereNotNull('end_valid_date')->get();
        if ($challenges_view->isEmpty()) {
            $result = [];
        } else {
            $result = collect([]);
            foreach ($challenges_view as $challenge_view) {
                $result->push($challenge_view);
            }
        }
        return view('challenges', ['challenges_view_result' => $result]);
    }

    public function deleteChallenge($id)
    {
        $elq = Challenge::find($id)->delete();
        $all_challenges = Challenge::all();
        return Redirect::route('challengesView', array('data' => $all_challenges));
    }

    public function editChallenge($id)
    {
        $elq = Challenge::find($id);
        return view('challenges-edit', ['data' => $elq]);
    }

    public function updateChallenge()
    {
        $name = Input::get('name');
        $desc = Input::get('description');
        $points = Input::get('points');
        $end_valid_date = Input::get('end_valid_date');
        $start_valid_date = Input::get('start_valid_date');
        $is_personal = Input::get('is_personal');
        $arrival_time = Input::get('arrival_time');
        $id = Input::get('id');
        $elq = Challenge::find($id);
        $elq->fill([
            'name' => $name,
            'description' => $desc,
            'points' => $points,
            'end_valid_date' => $end_valid_date,
            'start_valid_date' => $start_valid_date,
            'is_personal' => $is_personal,
            'arrival_time' => $arrival_time,
        ])->save();
        $all_challenges = Challenge::all();
        return view('challenges', [
            'challenges_view_result' => $all_challenges,
        ]);
    }
}
