<?php

namespace App\Http\Controllers;

use Illuminate\HttpRequest;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\User;
use App\Log_point;
use Carbon\Carbon;
use DB;

class Log_PointsController extends Controller
{
    #public static function store($user_id, $challenge_name, $points)
    #{
    #    $log_point = new Log_point();
    #    $log_point->user_id = $user_id;
    #    $log_point->challenge_name = $challenge_name;
    #    $log_point->points = $points;
    #    $success = $log_point->save();
    #    return Response::json($log_point, 201);
    #}

    public function getAllLog()
    {
        $all_Log = DB::table('log_points')
            ->leftJoin('users', 'log_points.user_id', '=', 'users.id')
            ->select('name', DB::raw('SUM(log_points.points) as total_points'),
                DB::raw('count(challenge_name) as subscribed_challenge'))
            ->groupBy('name')->orderBy('total_points','desc')->get();
        return view('leaderboard', [
            'data' => $all_Log,
        ]);
    }
}