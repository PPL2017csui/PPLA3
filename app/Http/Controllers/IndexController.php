<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\History;

class IndexController extends Controller
{
    public function index()
    {
        $year = Carbon::now()->year;
        $attendances = History::whereYear('created_at', '=', $year)
            ->whereNotNull('time_checkout')->get();
        $january = collect([]);
        $february = collect([]);
        $march = collect([]);
        $april = collect([]);
        $may = collect([]);
        $june = collect([]);
        $july = collect([]);
        $august = collect([]);
        $september = collect([]);
        $october = collect([]);
        $november = collect([]);
        $december = collect([]);
        $all = collect([]);
        foreach ($attendances as $attendance) {
            $month = $attendance->created_at->month;
            if ($month == 1) {
                $january->push($attendance);
            } else {
                if ($month == 2) {
                    $february->push($attendance);
                } else {
                    if ($month == 3) {
                        $march->push($attendance);
                    } else {
                        if ($month == 4) {
                            $april->push($attendance);
                        } else {
                            if ($month == 5) {
                                $may->push($attendance);
                            } else {
                                if ($month == 6) {
                                    $june->push($attendance);
                                } else {
                                    if ($month == 7) {
                                        $july->push($attendance);
                                    } else {
                                        if ($month == 8) {
                                            $august->push($attendance);
                                        } else {
                                            if ($month == 9) {
                                                $september->push($attendance);
                                            } else {
                                                if ($month == 10) {
                                                    $october->push($attendance);
                                                } else {
                                                    if ($month == 11) {
                                                        $november->push($attendance);
                                                    } else {
                                                        $december->push($attendance);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $all->push($january);
        $all->push($february);
        $all->push($march);
        $all->push($april);
        $all->push($may);
        $all->push($june);
        $all->push($july);
        $all->push($august);
        $all->push($september);
        $all->push($october);
        $all->push($november);
        $all->push($december);
        return view('index', [
            'data' => $all,
            'year' => $year,
        ]);
    }
}