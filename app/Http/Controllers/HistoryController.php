<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\History;
use App\User;
use Carbon\Carbon;
use App\User_Subscribe;
use App\Log_point;
use App\Challenge;

class HistoryController extends Controller
{
    public function index()
    {
        $attendances = History::whereDate('created_at', '=', Carbon::today()->toDateString())->get();
        if ($attendances->isEmpty()) {
            return Response::json("no attendances yet today", 200);
        } else {
            foreach ($attendances as $attendance) {
                $name = User::find($attendance->user_id)->name;
                $attendance->name = $name;
            }
            return Response::json($attendances, 200);
        }
    }

    public function submit()
    {
        $history = new History();
        $user_id = Input::get('user_id');
        $history->user_id = $user_id;
        $history->time_checkin = Carbon::now()->toDayDateTimeString();
        $history->time_checkout = null;
        $history->long = Input::get('long');
        $history->lat = Input::get('lat');
        $history->description = Input::get('description');
        $challenges = User_Subscribe::where('user_id', '=', $user_id)->where('is_subscribed', '=', true)->get();
        foreach ($challenges as $challenge) {
            $subscribedchallenges = Challenge::find($challenge->challenge_id);
            $date = Carbon::now()->toDateString();
            if ($date >= $subscribedchallenges->start_valid_date && $date <= $subscribedchallenges->end_valid_date) {
                if (Carbon::now()->toTimeString() <= $subscribedchallenges->arrival_time) {
                    $log_point = new Log_point();
                    $log_point->user_id = $user_id;
                    $log_point->challenge_name = $subscribedchallenges->name;
                    $log_point->points = $subscribedchallenges->points;
                    $log_point->save();
                }
            }
        }
        $success = $history->save();
        return Response::json($history, 201);
    }

    public function checkout()
    {
        $id = Input::get('id');
        $history = History::find($id);
        if (Carbon::now()->diffInMinutes(Carbon::parse($history->time_checkin)) >= 5) {
            $history->time_checkout = Carbon::now()->toDayDateTimeString();
            $success = $history->save();
            return Response::json("success", 201);
        } else {
            return Response::json("work at least for 5 minutes", 200);
        }
    }

    public function myhistory()
    {
        $user_id = Input::get('user_id');
        $year = Carbon::now()->year;
        $attendances = History::where('user_id', '=', $user_id)
            ->whereYear('created_at', '=', $year)
            ->whereNotNull('time_checkout')->get();
        if ($attendances->isEmpty()) {
            return Response::json("no attendances yet this year", 200);
        } else {
            $january = collect([]);
            $february = collect([]);
            $march = collect([]);
            $april = collect([]);
            $may = collect([]);
            $june = collect([]);
            $july = collect([]);
            $august = collect([]);
            $september = collect([]);
            $october = collect([]);
            $november = collect([]);
            $december = collect([]);
            $all = collect([]);
            foreach ($attendances as $attendance) {
                $month = $attendance->created_at->month;
                if ($month == 1) {
                    $january->push($attendance);
                } else {
                    if ($month == 2) {
                        $february->push($attendance);
                    } else {
                        if ($month == 3) {
                            $march->push($attendance);
                        } else {
                            if ($month == 4) {
                                $april->push($attendance);
                            } else {
                                if ($month == 5) {
                                    $may->push($attendance);
                                } else {
                                    if ($month == 6) {
                                        $june->push($attendance);
                                    } else {
                                        if ($month == 7) {
                                            $july->push($attendance);
                                        } else {
                                            if ($month == 8) {
                                                $august->push($attendance);
                                            } else {
                                                if ($month == 9) {
                                                    $september->push($attendance);
                                                } else {
                                                    if ($month == 10) {
                                                        $october->push($attendance);
                                                    } else {
                                                        if ($month == 11) {
                                                            $november->push($attendance);
                                                        } else {
                                                            $december->push($attendance);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $all->push($january);
            $all->push($february);
            $all->push($march);
            $all->push($april);
            $all->push($may);
            $all->push($june);
            $all->push($july);
            $all->push($august);
            $all->push($september);
            $all->push($october);
            $all->push($november);
            $all->push($december);
            return Response::json($all, 200);
        }
    }
}