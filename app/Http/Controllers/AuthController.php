<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Session;
class AuthController extends Controller
{
	
	
	public function doLogin(){
	
		$username = Input::get('username');
		$password = Input::get('password');
		
		if(($username == 'admin') && ($password == 'password0!')){
			Session::put('loggedIn', 'true');
			Session::save();
			return redirect('/index');
		}
		
		return view('login',[
			'error' => 'true'
		]);
		
	}
	
	public function doLogout(){
	
		Session::put('loggedIn', 'false');
		return view('login', [
			'error' => 'false'
		]);
	
	}
	
}
