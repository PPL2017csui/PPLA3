<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\User;
use App\Http\Controllers\Controller;
use App\User_Subscribe;
use App\Challenge;
use App\Log_point;
use Redirect;

class UserController extends Controller
{
    public function login()
    {
        $email = Input::get('email');
        $user = User::where('email', '=', $email)->get();
        if ($user->isEmpty()) {
            $newUser = new User([
                'birth_date' => Input::get('birth_date'),
                'name' => Input::get('name'),
                'email' => $email
            ]);
            $success = $newUser->save();
            return Response::json($newUser, 201);
        }
        return Response::json($user, 200);
    }

    public function editUser($id)
    {
        $elq = User::find($id);
        return view('employees-edit', ['data' => $elq]);
    }

    public function updateUser()
    {
        $name = Input::get('name');
        $email = Input::get('email');
        $role = Input::get('role');
        $birth_date = Input::get('birth_date');
        $phone = Input::get('phone');
        $id = Input::get('id');
        $elq = User::find($id);
        $elq->fill([
            'name' => $name,
            'email' => $email,
            'role' => $role,
            'birth_date' => $birth_date,
            'phone' => $phone,
        ])->save();
        $all_users = User::all();
        return view('employees', [
            'data' => $all_users,
        ]);
    }

    public function deleteUser($id)
    {
        $elq = User::find($id)->delete();
        $all_users = User::all();
        return Redirect::route('employeesView', array('data' => $all_users));
    }

    public function getAllUser()
    {
        $all_users = User::all();
        return view('employees', [
            'data' => $all_users,
        ]);
    }

    public function getProfile()
    {
        $id = Input::get('id');
        $user = User::find($id);
        return Response::json($user, 200);
    }

    public function setProfile()
    {
        $id = Input::get('id');
        $user = User::find($id);
        $user->name = Input::get('name');
        $user->phone = Input::get('phone');
        $success = $user->save();
        return Response::json($user, 201);
    }

    public function getPoints()
    {
        $user_id = Input::get('user_id');
        $log_points = Log_point::where('user_id', '=', $user_id)->get();
        $counter = 0;
        foreach ($log_points as $log_point) {
            $counter = $counter + $log_point->points;
        }
        return Response::json($counter, 200);
    }
}