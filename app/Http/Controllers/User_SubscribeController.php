<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Log_PointsController;
use App\Challenge;
use App\History;
use App\User;
use App\User_Subscribe;
use Carbon\Carbon;

class User_SubscribeController extends Controller
{
    #public function check_win()
    #{
    #    $user_id = Input::get('user_id');
    #    $challenge_id = Input::get('challenge_id');
    #    $date = new Carbon(Input::get('date'));
    #    $challenge = Challenge::where('id', '=', $challenge_id)->first();
    #    $attendances = History::where('user_id', '=', $user_id)->whereDate('time_checkin', '=', $date)->get();
    #    $arrival_time = new Carbon($challenge->arrival_time);
    #    $arrival_time->setDate($date->year, $date->month, $date->day);

    #    foreach ($attendances as $attendance) {
    #        $time_checkin = new Carbon($attendance->time_checkin);
    #        if ($time_checkin->lte($arrival_time)) {
    #            Log_PointsController::store($user_id, $challenge->name, $challenge->points);
    #            return Response::json("winning", 201);
    #        }
    #    }
    #    return Response::json("not winning", 201);
    #}

    public function subscribe()
    {
        $challenge_id = Input::get('challenge_id');
        $user_id = Input::get('user_id');
        $row = User_Subscribe::where('challenge_id', '=', $challenge_id)->where('user_id', '=', $user_id)->first();
        if ($row != null) {
            $row->is_subscribed = true;
            $success = $row->save();
            return Response::json($row, 201);
        } else {
            $user_subscribe = new User_Subscribe();
            $user_subscribe->challenge_id = $challenge_id;
            $user_subscribe->user_id = $user_id;
            $user_subscribe->is_subscribed = true;
            $success = $user_subscribe->save();
            return Response::json("success", 201);
        }
    }

    public function unsubscribe()
    {
        $challenge_id = Input::get('challenge_id');
        $user_id = Input::get('user_id');
        $user_subscribe = User_Subscribe::where('challenge_id',$challenge_id)->where('user_id',$user_id)->where('is_subscribed',true)->first();
        $user_subscribe->is_subscribed = false;
        $success = $user_subscribe->save();
        return Response::json("success", 201);
    }

    public function getSubscribers()
    {
        $challenge_id = Input::get('challenge_id');
        $subscribers = User_Subscribe::where('challenge_id', '=', $challenge_id)->where('is_subscribed', '=',
            true)->get();
        if ($subscribers->isEmpty()) {
            return Response::json("no subscribers yet", 200);
        } else {
            foreach ($subscribers as $subscriber) {
                $subscriber->name = User::find($subscriber->user_id)->name;
            }
            return Response::json($subscribers, 200);
        }
    }

    public function getSubscribedChallenges()
    {
        $user_id = Input::get('user_id');
        $challenges = User_Subscribe::where('user_id', '=', $user_id)->where('is_subscribed', '=', true)->get();
        if ($challenges->isEmpty()) {
            return Response::json("no challenges yet", 200);
        } else {
            foreach ($challenges as $challenge) {
                $elq = Challenge::find($challenge->challenge_id);
                $challenge->name = $elq->name;
                $challenge->desc = $elq->description;
                $challenge->points = $elq->points;
            }
            return Response::json($challenges, 200);
        }
    }
}