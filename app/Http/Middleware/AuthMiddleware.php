<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class AuthMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
		$loggedIn = Session::get('loggedIn');
		if($loggedIn == 'false'){
			return redirect('/');
		}
        return $next($request);
    }

}
