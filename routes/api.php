<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(array(''), function () {
    Route::resource('histories', 'HistoryController',
        array('except' => array('create', 'edit', 'destroy', 'show', 'update', 'store')));
});
Route::post('histories/submit', 'HistoryController@submit');
Route::post('histories/checkout', 'HistoryController@checkout');
Route::post('histories/myhistory', 'HistoryController@myhistory');

Route::post('user/login', 'UserController@login');
Route::post('user/getProfile', 'UserController@getProfile');
Route::post('user/setProfile', 'UserController@setProfile');
Route::post('user/getPoints', 'UserController@getPoints');

Route::group(array(''), function () {
    Route::resource('log_points', 'Log_pointsController',
        array('except' => array('create', 'edit', 'destroy', 'show', 'update')));
});

Route::group(array(''), function () {
    Route::resource('users_subscribers', 'User_SubscribeController',
        array('except' => array('create', 'edit', 'show', 'update', 'store')));
});
Route::post('users_subsribers/getSubscribedChallenges', 'User_SubscribeController@getSubscribedChallenges');
Route::post('users_subscribers/subscribe', 'User_SubscribeController@subscribe');
Route::post('users_subscribers/unsubscribe', 'User_SubscribeController@unsubscribe');
Route::post('users_subscribers/getSubscribers', 'User_SubscribeController@getSubscribers');
Route::post('users_subscribers/check_win', 'User_SubscribeController@check_win');

Route::group(array(''), function () {
    Route::resource('challenges', 'ChallengeController',
        array('except' => array('create', 'edit', 'destroy', 'show', 'update')));
});
