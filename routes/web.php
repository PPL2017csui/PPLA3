<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login', [
        'error' => 'false'
    ]);
});

Route::get('/index',
    [
        'as' => 'indexView',
        'uses' => 'IndexController@index'
    ])->middleware('dashboard_auth');

Route::get('/employees',
    [
        'as' => 'employeesView',
        'uses' => 'UserController@getAllUser'
    ])->middleware('dashboard_auth');

Route::get('/employees/{id}',
    [
        'as' => 'employeesEdit',
        'uses' => 'UserController@editUser'
    ])->middleware('dashboard_auth');

Route::get('/employees-delete/{id}',
    [
        'as' => 'employeesDelete',
        'uses' => 'UserController@deleteUser'
    ])->middleware('dashboard_auth');

Route::post('/employees',
    [
        'as' => 'employeesUpdate',
        'uses' => 'UserController@updateUser'
    ])->middleware('dashboard_auth');

Route::get('/leaderboard',
    [
        'as' => 'leaderboadView',
        'uses' => 'Log_pointsController@getAllLog'
    ])->middleware('dashboard_auth');


Route::get('/',
    [
        'as' => 'doLogout',
        'uses' => 'AuthController@doLogout'
    ]);

Route::post('/',
    [
        'as' => 'login',
        'uses' => 'AuthController@doLogin'
    ]);


Route::get('/challenges',
    [
    	'as' => 'challengesView',
    	'uses' => 'ChallengeController@challenge_view'
    ])->middleware('dashboard_auth');


Route::get('/challenges-edit/{id}',
    [
        'as' => 'challengesEdit',
        'uses' => 'ChallengeController@editChallenge'
    ])->middleware('dashboard_auth');


Route::get('/challenges-delete/{id}',
    [
        'as' => 'challengesDelete',
        'uses' => 'ChallengeController@deleteChallenge'
    ])->middleware('dashboard_auth');

Route::post('/challenges',
    [
        'as' => 'challengesUpdate',
        'uses' => 'ChallengeController@updateChallenge'
    ])->middleware('dashboard_auth');