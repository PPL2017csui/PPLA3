
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Jojonomic Attendance Gamification Dashboard" content="">
    <meta name="PPLA3" content="">

    <link rel="shortcut icon" type="image/x-icon" href="../assets/icon.jpg" />
    
    <title>Jojonomic Attendance Gamification Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 500px;
      }
    </style>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Jojonomic Attendance Gamification Dashboard</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li><a href="{{ url('/') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ url('/index') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/employees') }}"><i class="fa fa-table fa-fw"></i> Employees</a>
                        </li>
                        <li>
                            <a href="{{ url('/challenges') }}"><i class="fa fa-table fa-fw"></i> Challenges</a>
                        </li>
                         <li>
                            <a href="{{ url('/leaderboard') }}"><i class="fa fa-table fa-fw"></i> Leaderboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/office') }}"><i class="fa fa-table fa-fw"></i> Company</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form role="form" method="POST" action="{{ route('officeUpdate') }}">
                                {{ csrf_field() }}
                                <fieldset>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Name <span class="text-danger">*</span></label>
                                            <input class="form-control" placeholder="Name" name="name" type="text" value="{{ $data->name }}">
                                        </div>
                                        <input type='hidden' name="lat" value="{{ $data->lat }}" id="lat">
                                        <input type='hidden' name="lng" value="{{ $data->lng }}" id="lng">
                                        <div class="form-group">
                                            <div id="map"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 right">
                                        <!-- Change this to a button or input when using this as a form -->
                                        <button type="submit" class="btn btn-lg btn-success btn-block">Submit Data</button>
                                    </div>
                                </div>
                                </fieldset>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="css/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="css/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="css/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="css/dist/js/sb-admin-2.js"></script>

    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC_ZQ0PU1gy1j96NE_UAtK3Jf0YtmSl8SA&callback=initMap"
            async defer></script>

    <script>
            var map;
            var longpress = false;
            var markers = [];
            function initMap() {

                map = new google.maps.Map(document.getElementById('map'), {
                    center: { lat: {{ $data->lat }}, lng: {{$data->lng}} },
                    zoom: 18
                });

                marker = new google.maps.Marker({
                  position: { lat: {{ $data->lat }}, lng: {{$data->lng}} },
                  map: map,
                  title: 'Company\'s Location'
                });
                markers.push(marker);

                function setMapNull(){
                    markers[0].setMap(null);
                    markers = [];
                }

                function pushMap(map){
                    markers.push(map);
                }

                google.maps.event.addListener(map,'click', function (event) {
                        if(longpress){
                            setMapNull();
                            document.getElementById("lat").value = event.latLng.lat();
                            document.getElementById("lng").value = event.latLng.lng();
                            var marker = new google.maps.Marker({
                              position: event.latLng,
                              map: map,
                              title: 'Company\'s Location'
                            });
                            pushMap(marker);
                        }
                        //(longpress) ? console.log("Long Press") : console.log("Short Press");
                });

                google.maps.event.addListener(map, 'mousedown', function(event){
                        start = new Date().getTime();           
                });

                google.maps.event.addListener(map, 'mouseup', function(event){
                        end = new Date().getTime();
                        longpress = (end - start < 500) ? false : true;         
                });
            }
    </script>

</body>

</html>

