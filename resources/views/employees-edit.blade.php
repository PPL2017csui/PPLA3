<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Jojonomic Attendance Gamification Dashboard" content="">
    <meta name="PPLA3" content="">

    <title>Jojonomic Attendance Gamification Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../css/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="../css/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../css/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Jojonomic Attendance Gamification Dashboard</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">                   
                <li><a href="{{ url('/') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ url('/index') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/employees') }}"><i class="fa fa-table fa-fw"></i> Employees</a>
                        </li>
                        <li>
                            <a href="{{ url('/challenges') }}"><i class="fa fa-table fa-fw"></i> Challenges</a>
                        </li>
                         <li>
                            <a href="{{ url('/leaderboard') }}"><i class="fa fa-table fa-fw"></i> Leaderboard</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Jojonomic's Employee</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form role="form" method="POST" action="{{ route('employeesUpdate') }}">
								{{ csrf_field() }}
								<fieldset>
								<input type="hidden" name="id" value="{{ $data->id }}">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>Name <span class="text-danger">*</span></label>
											<input class="form-control" placeholder="Name" name="name" type="text" value="{{ $data->name }}">
										</div>
										<div class="form-group">
											<label>Email <span class="text-danger">*</span></label>
											<input class="form-control" placeholder="Email" name="email" value="{{ $data->email }}">
										</div>
										<div class="form-group">
											<label>Role <span class="text-danger">*</span></label><br>
											<select name="role">		
											@if($data->role == 'admin')
												<option value="admin" selected="selected">Admin</option>
											@else
												<option value="admin">Admin</option>
											@endif
											@if($data->role == 'employee')
												<option value="employee" selected="selected">Employee</option>
											@else
												<option value="employee" >Employee</option>
											@endif
											@if($data->role == 'supervisor')
												<option value="supervisor" selected="selected">Supervisor</option>
											@else
												<option value="supervisor">Supervisor</option>
											@endif
											</select>
											<!--<input class="form-control" placeholder="Role" name="Role" value="{{ $data->role }}">-->
										</div>
										<div class="form-group">
											<label>Birth Date <span class="text-danger">*</span></label>
											<input placeholder="Birth Date" name="birth_date" value="{{ $data->birth_date }}" class="form-control datepicker">
										</div>
										<div class="form-group">
											<label>Phone <span class="text-danger">*</span></label>
											<input class="form-control" placeholder="Phone" name="phone" value="{{ $data->phone }}">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4 right">
										<!-- Change this to a button or input when using this as a form -->
										<button type="submit" class="btn btn-lg btn-success btn-block">Submit Data</button>
									</div>
								</div>
								</fieldset>
							</form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
	
    <!-- jQuery -->
    <script src="../css/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../css/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../css/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="../css/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="../css/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="../css/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../css/dist/js/sb-admin-2.js"></script>
	
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
	  $(function() {
		$( ".datepicker" ).datepicker();
	  });
	</script>
	
    <!-- Page-Level Demo Scripts - Tables - Use for reference-->
    <script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>
