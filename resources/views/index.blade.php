@extends('layout.header')
	@section('css')
	<!-- Morris Charts CSS -->
    <link href="css/vendor/morrisjs/morris.css" rel="stylesheet">
	@endsection
	@section('content')
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Attendance Summary</h1>
                </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> 
                            <div class="pull-right">
                                <!--<div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">Action</a>
                                        </li>
                                    </ul>
                                </div>-->
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="morris-area-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                 
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
					
                @endsection
				@section('js-data')
				<!-- Morris Charts JavaScript -->
				<script src="css/vendor/raphael/raphael.min.js"></script>
				<script src="css/vendor/morrisjs/morris.min.js"></script>
				<!--<script src="css/data/morris-data.js"></script>-->
				<script type="text/javascript">
					
					$(function() {

						Morris.Area({
							element: 'morris-area-chart',
							data: [{
								period: '2017-01',
								attendance: {{ $data[0]->count() }},
							}, {
								period: '2017-02',
								attendance: {{ $data[1]->count() }},
							}, {
								period: '2017-03',
								attendance: {{ $data[2]->count() }},
							}, {
								period: '2017-04',
								attendance: {{ $data[3]->count() }},
							}, {
								period: '2017-05',
								attendance: {{ $data[4]->count() }},
							}, {
								period: '2017-06',
								attendance: {{ $data[5]->count() }},
							}, {
								period: '2017-07',
								attendance: {{ $data[6]->count() }},
							}, {
								period: '2017-08',
								attendance: {{ $data[7]->count() }},
							}, {
								period: '2017-09',
								attendance: {{ $data[8]->count() }},
							}, {
								period: '2017-10',
								attendance: {{ $data[9]->count() }},
							}, {
								period: '2017-11',
								attendance: {{ $data[10]->count() }},
							}, {
								period: '2017-12',
								attendance: {{ $data[11]->count() }},
							}],
							xkey: 'period',
							ykeys: ['attendance'],
							labels: ['attendance'],
							pointSize: 2,
							hideHover: 'auto',
							resize: true
						});
						
					});
					
					</script>
				@endsection
