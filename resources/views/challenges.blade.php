
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Jojonomic Attendance Gamification Dashboard" content="">
    <meta name="PPLA3" content="">

    <title>Jojonomic Attendance Gamification Dashboard</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Jojonomic Attendance Gamification Dashboard</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li><a href="{{ url('/') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ url('/index') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/employees') }}"><i class="fa fa-table fa-fw"></i> Employees</a>
                        </li>
                        <li>
                            <a href="{{ url('/challenges') }}"><i class="fa fa-table fa-fw"></i> Challenges</a>
                        </li>
                         <li>
                            <a href="{{ url('/leaderboard') }}"><i class="fa fa-table fa-fw"></i> Leaderboard</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">This Month's Challenge</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        
            <div class="row">
                <!-- /.col-lg-4 -->
				@if(count($challenges_view_result) > 0)
					@foreach ($challenges_view_result as $challenge_view)
				
					<div class="col-lg-11">
						<div class="panel panel-primary">
							<div class="panel-heading">
							  <h1> {{ $challenge_view -> name }} </h1>
							</div>
							<div class="panel-body">
							   <p>{{ $challenge_view -> description }}</p>
                               <a href="{{ route('challengesEdit', ['id'=>$challenge_view->id]) }}">Edit</a>
                               <a href="{{ route('challengesDelete', ['id'=>$challenge_view->id]) }}">Delete</a></td>
							</div>
						</div>
					</div>
				   @endforeach
				@else
					<div class="col-lg-11">
						<div class="panel panel-primary">
							<div class="panel-heading">
							  <h1> Sorry! </h1>
							</div>
							<div class="panel-body">
							   <p>There is no challenges this month</p>
							</div>
						</div>
					</div>
			   @endif
            <!-- /.row -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="css/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="css/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="css/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="css/dist/js/sb-admin-2.js"></script>

</body>

</html>

